@component('mail::message')
# Confirm your email

Hello,

To validate your account, we need you to confirm your email address.

@component('mail::button', ['url' => route('register.confirm', ['token' => $user->confirm_token])])
Validate my account
@endcomponent

See you soon !<br>
{{ config('app.name') }}

@component('mail::subcopy')
@lang(
    "If you’re having trouble clicking the \":actionText\" button, copy and paste the URL below\n".
    'into your web browser: [:actionURL](:actionURL)',
    [
        'actionText' => 'Validate my account',
        'actionURL' => route('register.confirm', ['token' => $user->confirm_token])
    ]
)
@endcomponent
@endcomponent
