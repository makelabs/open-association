@component('mail::message')
# Hi, {{ $user->name }} !

Your email address has been change to {{ $email }}.

If you did not changed your email address, please contact us as soon as possible.

Thanks,<br>
{{ config('app.name') }}
@endcomponent
