@component('mail::message')
# Hi, {{ $user->name }} !

@component('mail::button', ['url' => config('app.url')])
Welcome to Open Association
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
