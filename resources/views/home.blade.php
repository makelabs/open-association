@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">

            @if (session('success'))
                <div class="alert alert-success" role="alert">
                    {{ session('success') }}
                </div>
            @endif
            @if (session('error'))
                <div class="alert alert-danger" role="alert">
                    {{ session('error') }}
                </div>
            @endif

            <div class="card">
                <div class="card-header">My documents</div>

                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">Name</th>
                                <th scope="col">Description</th>
                                <th scope="col">Created at</th>
                                <th scope="col">Opérations</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($documents as $document)
                            <tr>
                                <th scope="row"><a target="_blank" href="{{ route('documents.download', ['id' => $document->id]) }}">{{ $document->name }}</a></th>
                                <td>{{ $document->description }}</td>
                                <td>{{ $document->created_at->format('Y-m-d') }}</td>
                                <td>
                                    @can('update', $document)
                                    <a class="btn btn-info btn-sm" role="button" href="{{ route('documents.edit', ['id' => $document->id]) }}">Edit</a>
                                    @endcan
                                    @can('delete', $document)
                                    <form style="display:inline-block" method="POST" action="{{ route('documents.destroy', ['id' => $document->id]) }}" aria-label="{{ __('Delete user') }}">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-danger btn-sm" type="submit">Delete</button>
                                    </form>
                                    @endcan
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
