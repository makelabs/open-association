@extends('layouts.app')

@section('content')
<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-10">

            <div class="card mb-3">

                @foreach ($errors->all() as $error)
                    <div>{{ $error }}</div>
                @endforeach

                <div class="card-header">{{ __('Create topic') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('topics.store') }}" aria-label="{{ __('Create topic') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="title" class="col-md-4 col-form-label text-md-right">{{ __('Title') }}</label>

                            <div class="col-md-6">
                                <input id="title" type="text" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" name="title" value="{{ old('title') }}" required autofocus>

                                @if ($errors->has('title'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="description" class="col-md-4 col-form-label text-md-right">{{ __('Description') }}</label>

                            <div class="col-md-6">
                                <textarea id="description" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" required>{{ old('description') }}</textarea>

                                @if ($errors->has('description'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="closed_at" class="col-md-4 col-form-label text-md-right">{{ __('Closed at') }}</label>

                            <div class="col-md-6">
                                <input type="date" id="closed_at" class="form-control{{ $errors->has('closed_at') ? ' is-invalid' : '' }}" name="closed_at" value="{{ old('closed_at') }}" required>

                                @if ($errors->has('closed_at'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('closed_at') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row" id="action-form">
                            <label for="action" class="col-md-4 col-form-label text-md-right">{{ __('Action') }}</label>

                            <div class="col-md-6">
                                <select class="form-control{{ $errors->has('action') ? ' is-invalid' : '' }}" id="action" name="action" required>
                                    <option selected>Choose ...</option>
                                    <option value="poll" {{ old('action') == 'poll' ? 'selected' : '' }}>Poll</option>
                                    <option value="proposal" {{ old('action') == 'proposal' ? 'selected' : '' }}>Proposal</option>
                                </select>

                                @if ($errors->has('action'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('action') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row" id="options-form">
                            <label for="options" class="col-md-4 col-form-label text-md-right">{{ __('Options') }}</label>

                            <div class="col-md-6" id="options-content">
                                @if (old('options'))
                                @foreach (old('options') as $optionKey => $optionValue)
                                <div class="input-group mt-3">
                                    <input id="options" type="text" class="form-control{{ $errors->has('options') ? ' is-invalid' : '' }}" name="options[]" value="{{ $optionValue }}">
                                    @if ($optionKey == 0)
                                    <div class="input-group-append" id="options-add">
                                        <button class="btn btn-outline-secondary" type="button">Add option</button>
                                    </div>
                                    @else
                                    <div class="input-group-append" id="options-remove">
                                        <button class="btn btn-outline-secondary" type="button">Remove option</button>
                                    </div>
                                    @endif
                                </div>
                                @endforeach
                                @else
                                <div class="input-group">
                                    <input id="options" type="text" class="form-control{{ $errors->has('options') ? ' is-invalid' : '' }}" name="options[]">
                                    <div class="input-group-append" id="options-add">
                                        <button class="btn btn-outline-secondary" type="button">Add option</button>
                                    </div>
                                </div>
                                @endif
                            </div>

                             @if ($errors->has('options'))
                                <div class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('options') }}</strong>
                                </div>
                            @endif
                        </div>

                        <div class="form-group row">
                            <label for="tags" class="col-md-4 col-form-label text-md-right">{{ __('Tags') }}</label>

                            <div class="col-md-6">
                                <input id="tags" type="text" class="form-control{{ $errors->has('tags') ? ' is-invalid' : '' }}" name="tags" value="{{ old('tags') }}">

                                @if ($errors->has('tags'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('tags') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row" id="options-multiple">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input{{ $errors->has('is_multiple') ? ' is-invalid' : '' }}" type="checkbox" name="is_multiple" id="is_multiple" {{ old('is_multiple') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="is_multiple">
                                        {{ __('Multiple') }}
                                    </label>

                                    @if ($errors->has('is_multiple'))
                                        <div class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('is_multiple') }}</strong>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input{{ $errors->has('is_anonymous') ? ' is-invalid' : '' }}" type="checkbox" name="is_anonymous" id="is_anonymous" {{ old('is_anonymous') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="is_anonymous">
                                        {{ __('Anonymous') }}
                                    </label>

                                    @if ($errors->has('is_anonymous'))
                                        <div class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('is_anonymous') }}</strong>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input{{ $errors->has('hide_results') ? ' is-invalid' : '' }}" type="checkbox" name="hide_results" id="hide_results" {{ old('hide_results') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="hide_results">
                                        {{ __('Hide results') }}
                                    </label>

                                    @if ($errors->has('hide_results'))
                                        <div class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('hide_results') }}</strong>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Add') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@javascript('tags', $tags)

@push('scripts')
<script type="text/javascript">

    $(document).ready(function () {
        const optionsInput = '<div class="input-group mt-3"><input id="options" type="text" class="form-control" name="options[]"><div class="input-group-append" id="options-remove"><button class="btn btn-outline-secondary" type="button">Delete option</button></div></div>';

        if ($('#action').get(0).value != 'poll') {
            $('#options-form').hide();
            $('#options-multiple').hide();
        }

        $('#action').change(function (event) {
            const action = event.target.value;

            switch (action) {
                case 'poll':
                    $('#options-form').show();
                    $('#options-multiple').show();
                    break;
                case 'proposal':
                    $('#options-form').hide();
                    $('#options-multiple').hide();
                    break;
                default:
                    $('#options-form').hide();
                    $('#options-multiple').hide();
                    break;
            }
        });

        $('#options-add').click(function (event) {
            $('#options-content').append(optionsInput);
        });

        $(document).on('click', '#options-remove', function (event) {
            const buttonGroup = $(event.target).parent();
            const inputGroup = $(buttonGroup).parent();
            inputGroup.remove();
        });

        const availableTags = tags;

        function split(val) {
            return val.split(/,\s*/);
        }
        function extractLast(term) {
            return split(term).pop();
        }

        $("#tags").on("keydown", function(event) {

            if (event.keyCode === $.ui.keyCode.TAB && $(this).autocomplete("instance").menu.active) {
                event.preventDefault();
            }

        }).autocomplete({
            minLength: 0,
            source: function(request, response) {
                // delegate back to autocomplete, but extract the last term
                response($.ui.autocomplete.filter(availableTags, extractLast(request.term)));
            },
            focus: function() {
                // prevent value inserted on focus
                return false;
            },
            select: function(event, ui) {
                var terms = split(this.value);
                // remove the current input
                terms.pop();
                // add the selected item
                terms.push(ui.item.value);
                // add placeholder to get the comma-and-space at the end
                terms.push("");
                this.value = terms.join(", ");
                return false;
            }
        }).focus(function () {
            $(this).autocomplete("search");
        });

    });

</script>
@endpush
