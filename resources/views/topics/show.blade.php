@extends('layouts.app')

@section('content')
<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-10">

            @if (session('success'))
                <div class="alert alert-success" role="alert">
                    {{ session('success') }}
                </div>
            @endif

            @if (session('error'))
                <div class="alert alert-danger" role="alert">
                    {{ session('error') }}
                </div>
            @endif

            {{ Breadcrumbs::render('topic', $topic) }}

            <div class="card mb-3">
                <div class="card-body">
                    <dl class="row">
                        <dt class="col-sm-2">Title</dt>
                        <dd class="col-sm-10">{{ $topic->title }}</dd>

                        <dt class="col-sm-2">Description</dt>
                        <dd class="col-sm-10">{{ $topic->description }}</dd>

                        @if ($topic->tags->count() > 0)
                        <dt class="col-sm-2">Tags</dt>
                        <dd class="col-sm-10">
                            @foreach ($topic->tags as $tag)
                                <a href="{{ route('tags.show', ['slug' => $tag->slug]) }}" class="badge badge-primary">{{ $tag->name }}</a>
                            @endforeach
                        </dd>
                        @endif

                        <dt class="col-sm-2">Type</dt>
                        <dd class="col-sm-10">{{ ucfirst($topic->type) }}</dd>

                        <dt class="col-sm-2">Anonymous</dt>
                        <dd class="col-sm-10">{{ ($topic->is_anonymous) ? 'Yes' : 'No' }}</dd>

                        <dt class="col-sm-2">Multiple choices</dt>
                        <dd class="col-sm-10">{{ ($topic->is_multiple) ? 'Yes' : 'No' }}</dd>

                        <dt class="col-sm-2">Created at</dt>
                        <dd class="col-sm-10">{{ $topic->created_at->format('Y-m-d H:i:s') }}</dd>

                        <dt class="col-sm-2">Closed at</dt>
                        <dd class="col-sm-10">{{ $topic->closed_at->format('Y-m-d H:i:s') }}</dd>

                        <dt class="col-sm-2">Participation</dt>
                        <dd class="col-sm-10">{{ $participation['pourcentage'] }} % of members have stated their position ({{ $participation['votesCount'] }}/{{ $participation['usersCount'] }})</dd>

                        <dt class="col-sm-2">Results</dt>
                        <dd class="col-sm-10">
                            @can('results', $topic)
                                @if ($topic->type == 'proposal')
                                    @foreach ($results as $resultKey => $resultValue)
                                    <div class="progress mb-3">
                                        <div class="progress-bar bg-success" role="progressbar" style="width: {{ $resultValue }}%;" aria-valuenow="{{ $resultValue }}" aria-valuemin="0" aria-valuemax="100">{{ ucfirst($resultKey) }} ({{ $resultValue }}%)</div>
                                    </div>
                                    @endforeach
                                @elseif ($topic->type == 'poll')
                                @foreach ($results as $resultKey => $resultValue)
                                <div class="progress mb-3">
                                    <div class="progress-bar bg-success" role="progressbar" style="width: {{ $resultValue }}%;" aria-valuenow="{{ $resultValue }}" aria-valuemin="0" aria-valuemax="100">{{ ucfirst($resultKey) }} ({{ $resultValue }}%)</div>
                                </div>
                                @endforeach
                                @else
                                    Nothing to show ({{ $topic->type }}).
                                @endif
                            @endcan
                        </dd>

                        <dt class="col-sm-2">Action</dt>
                        <dd class="col-sm-10">
                            @can('vote', $topic)
                                <form method="post" action="{{ route('topics.vote', ['id' => $topic->id]) }}" aria-label="{{ __('Vote topic') }}">
                                    @csrf
                                    @if ($topic->is_multiple == true)
                                        @foreach ($topic->options as $option)
                                            <div class="form-group form-check form-check-inline">
                                                <input class="form-check-input" type="checkbox" name="choices[]" value="{{ $option->id }}" id="{{ $option->id }}">
                                                <label class="form-check-label" for="{{ $option->id }}">
                                                    {{ ucfirst($option->value) }}
                                                </label>
                                            </div>
                                        @endforeach
                                    @else
                                        @foreach ($topic->options as $option)
                                            <div class="form-group form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="choices[]" value="{{ $option->id }}" id="{{ $option->id }}">
                                                <label class="form-check-label" for="{{ $option->id }}">
                                                    {{ ucfirst($option->value) }}
                                                </label>
                                            </div>
                                        @endforeach
                                    @endif
                                    <div class="form-group form-check">
                                        <input class="form-check-input{{ $errors->has('is_anonymous') ? ' is-invalid' : '' }}" type="checkbox" name="is_anonymous" id="is_anonymous" {{ old('is_anonymous') ? 'checked' : '' }}>

                                        <label class="form-check-label" for="is_anonymous">
                                            {{ __('I choose to vote anonymously (You won\'t be able to change your statement).') }}
                                        </label>

                                        @if ($errors->has('is_anonymous'))
                                            <div class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('is_anonymous') }}</strong>
                                            </div>
                                        @endif
                                    </div>
                                    <button class="btn btn-primary" type="submit">Vote</button>
                                </form>
                            @endcan
                            @can('closed', $topic)
                                Topic is closed.
                            @endcan
                            @cannot('vote', $topic)
                                @if ($userVoteOptions)
                                    You choosed :
                                    @foreach ($userVoteOptions as $option)
                                        <span class="badge badge-info">{{ ucfirst($option->value) }}</span>
                                    @endforeach
                                @else
                                    You already stated.
                                @endif
                            @endcan
                        </dd>
                    </dl>
                    <div class="btn-toolbar" role="toolbar" aria-label="Topic actions">
                        @can('update', $topic)
                        <div class="btn-group mr-2" role="group" aria-label="Edit topic">
                            <a class="btn btn-info" role="button" href="{{ route('topics.edit', ['id' => $topic->id]) }}">Edit</a>
                        </div>
                        @endcan
                        @can('delete', $topic)
                        <div class="btn-group" role="group" aria-label="Delete topic">
                            <form style="display:inline-block" method="POST" action="{{ route('topics.destroy', ['id' => $topic->id]) }}" aria-label="{{ __('Delete topic') }}">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger" type="submit">Delete</button>
                            </form>
                        </div>
                        @endcan
                    </div>
                </div>
            </div>
            @if ($topic->is_anonymous == false)
                <div class="card mb-3">
                    <div class="card-body">
                        <h5 class="card-title">Activity</h5>
                        <ul class="list-group list-group-flush">
                            @if ($topic->votes->count() > 0)
                            @foreach ($topic->votes as $vote)
                                @if ($vote->user)
                                    <li class="list-group-item"><strong>{{ $vote->user->name }}</strong> choosed <strong>{{ ucfirst($vote->option->value) }}</strong> at <strong>{{ $vote->created_at->format('Y-m-d H:i:s') }}</strong>.</li>
                                @else
                                    <li class="list-group-item"><strong>Someone</strong> choosed <strong>{{ ucfirst($vote->option->value) }}</strong> at <strong>{{ $vote->created_at->format('Y-m-d H:i:s') }}</strong>.</li>
                                @endif
                            @endforeach
                            @else
                                <li class="list-group-item">There is no activity there.</li>
                            @endif
                        </ul>
                    </div>
                </div>
            @endif
        </div>
    </div>
</div>
@endsection
