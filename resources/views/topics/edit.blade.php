@extends('layouts.app')

@section('content')
<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-10">

            {{ Breadcrumbs::render('topic', $topic) }}

            <div class="card mb-3">

                @foreach ($errors->all() as $error)
                    <div>{{ $error }}</div>
                @endforeach


                <div class="card-header">{{ __('Edit topic') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('topics.update', ['id' => $topic->id]) }}" aria-label="{{ __('Edit topic') }}">
                        @csrf
                        @method('PUT')

                        <div class="form-group row">
                            <label for="title" class="col-md-2 col-form-label text-md-right">{{ __('Title') }}</label>

                            <div class="col-md-8">
                                <input id="title" type="text" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" name="title" value="{{ $topic->title }}" required autofocus>

                                @if ($errors->has('title'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="description" class="col-md-2 col-form-label text-md-right">{{ __('Description') }}</label>

                            <div class="col-md-8">
                                <textarea id="description" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" required>{{ $topic->description }}</textarea>

                                @if ($errors->has('description'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="closed_at" class="col-md-2 col-form-label text-md-right">{{ __('Closed at') }}</label>

                            <div class="col-md-8">
                                <input type="date" id="closed_at" class="form-control{{ $errors->has('closed_at') ? ' is-invalid' : '' }}" name="closed_at" value="{{ $topic->closed_at->format('Y-m-d') }}" required>

                                @if ($errors->has('closed_at'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('closed_at') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        @if ($topic->isEditable() == false)
                        <div class="offset-2 col-md-8">
                            <div class="alert alert-warning" role="alert">
                                There have already been votes on this topic, some inputs can not be changed.
                            </div>
                        </div>
                        @endif

                        <div class="form-group row" id="action-form">
                            <label for="action" class="col-md-2 col-form-label text-md-right">{{ __('Action') }}</label>

                            <div class="col-md-8">
                                <select class="form-control{{ $errors->has('action') ? ' is-invalid' : '' }}" id="action" name="action" required {{ $topic->isEditable() ? '' : 'disabled' }}>
                                    <option selected>Choose ...</option>
                                    <option value="poll" {{ $topic->type == 'poll' ? 'selected' : '' }}>Poll</option>
                                    <option value="proposal" {{ $topic->type == 'proposal' ? 'selected' : '' }}>Proposal</option>
                                </select>

                                @if ($errors->has('action'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('action') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row" id="options-form">
                            <label for="options" class="col-md-2 col-form-label text-md-right">{{ __('Options') }}</label>

                            <div class="col-md-8" id="options-content">
                                @if ($topic->options)
                                @foreach ($topic->options as $optionKey => $optionColleciton)
                                <div class="input-group mt-3">
                                    <input id="options" type="text" class="form-control{{ $errors->has('options') ? ' is-invalid' : '' }}" name="options[]" value="{{ $optionColleciton->value }}" {{ $topic->isEditable() ? '' : 'disabled' }}>
                                    @if ($optionKey == 0)
                                    <div class="input-group-append" id="options-add">
                                        <button class="btn btn-outline-secondary" type="button" {{ $topic->isEditable() ? '' : 'disabled' }}>Add option</button>
                                    </div>
                                    @else
                                    <div class="input-group-append" id="options-remove">
                                        <button class="btn btn-outline-secondary" type="button" {{ $topic->isEditable() ? '' : 'disabled' }}>Remove option</button>
                                    </div>
                                    @endif
                                </div>
                                @endforeach
                                @else
                                <div class="input-group">
                                    <input id="options" type="text" class="form-control{{ $errors->has('options') ? ' is-invalid' : '' }}" name="options[]" {{ $topic->isEditable() ? '' : 'disabled' }}>
                                    <div class="input-group-append" id="options-add">
                                        <button class="btn btn-outline-secondary" type="button">Add option</button>
                                    </div>
                                </div>
                                @endif
                            </div>

                             @if ($errors->has('options'))
                                <div class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('options') }}</strong>
                                </div>
                            @endif
                        </div>

                        <div class="form-group row">
                            <label for="tags" class="col-md-2 col-form-label text-md-right">{{ __('Tags') }}</label>

                            <div class="col-md-8">
                                <input id="tags" type="text" class="form-control{{ $errors->has('tags') ? ' is-invalid' : '' }}" name="tags" value="{{ $topic->tags->implode('name', ', ') }}">

                                @if ($errors->has('tags'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('tags') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row" id="options-multiple">
                            <div class="col-md-8 offset-md-2">
                                <div class="form-check">
                                    <input class="form-check-input{{ $errors->has('is_multiple') ? ' is-invalid' : '' }}" type="checkbox" name="is_multiple" id="is_multiple" {{ $topic->is_multiple ? 'checked' : '' }} {{ $topic->isEditable() ? '' : 'disabled' }}>

                                    <label class="form-check-label" for="is_multiple">
                                        {{ __('Multiple') }}
                                    </label>

                                    @if ($errors->has('is_multiple'))
                                        <div class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('is_multiple') }}</strong>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-8 offset-md-2">
                                <div class="form-check">
                                    <input class="form-check-input{{ $errors->has('is_anonymous') ? ' is-invalid' : '' }}" type="checkbox" name="is_anonymous" id="is_anonymous" {{ $topic->is_anonymous ? 'checked' : '' }} {{ $topic->isEditable() ? '' : 'disabled' }}>

                                    <label class="form-check-label" for="is_anonymous">
                                        {{ __('Anonymous') }}
                                    </label>

                                    @if ($errors->has('is_anonymous'))
                                        <div class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('is_anonymous') }}</strong>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-8 offset-md-2">
                                <div class="form-check">
                                    <input class="form-check-input{{ $errors->has('hide_results') ? ' is-invalid' : '' }}" type="checkbox" name="hide_results" id="hide_results" {{ $topic->hide_results ? 'checked' : '' }} {{ $topic->isEditable() ? '' : 'disabled' }}>

                                    <label class="form-check-label" for="hide_results">
                                        {{ __('Hide results') }}
                                    </label>

                                    @if ($errors->has('hide_results'))
                                        <div class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('hide_results') }}</strong>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-2">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Edit') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@javascript('tags', $tags)

@push('scripts')
<script type="text/javascript">

    $(document).ready(function () {
        const optionsInput = '<div class="input-group mt-3"><input id="options" type="text" class="form-control" name="options[]"><div class="input-group-append" id="options-remove"><button class="btn btn-outline-secondary" type="button">Delete option</button></div></div>';

        if ($('#action').get(0).value != 'poll') {
            $('#options-form').hide();
            $('#options-multiple').hide();
        }

        $('#action').change(function (event) {
            const action = event.target.value;

            switch (action) {
                case 'poll':
                    $('#options-form').show();
                    $('#options-multiple').show();
                    break;
                case 'proposal':
                    $('#options-form').hide();
                    $('#options-multiple').hide();
                    break;
                default:
                    $('#options-form').hide();
                    $('#options-multiple').hide();
                    break;
            }
        });

        $('#options-add').click(function (event) {
            $('#options-content').append(optionsInput);
        });

        $(document).on('click', '#options-remove', function (event) {
            const buttonGroup = $(event.target).parent();
            const inputGroup = $(buttonGroup).parent();
            inputGroup.remove();
        });

        const availableTags = tags;

        function split(val) {
            return val.split(/,\s*/);
        }
        function extractLast(term) {
            return split(term).pop();
        }

        $("#tags").on("keydown", function(event) {

            if (event.keyCode === $.ui.keyCode.TAB && $(this).autocomplete("instance").menu.active) {
                event.preventDefault();
            }

        }).autocomplete({
            minLength: 0,
            source: function(request, response) {
                // delegate back to autocomplete, but extract the last term
                response($.ui.autocomplete.filter(availableTags, extractLast(request.term)));
            },
            focus: function() {
                // prevent value inserted on focus
                return false;
            },
            select: function(event, ui) {
                var terms = split(this.value);
                // remove the current input
                terms.pop();
                // add the selected item
                terms.push(ui.item.value);
                // add placeholder to get the comma-and-space at the end
                terms.push("");
                this.value = terms.join(", ");
                return false;
            }
        }).focus(function () {
            $(this).autocomplete("search");
        });

    });

</script>
@endpush
