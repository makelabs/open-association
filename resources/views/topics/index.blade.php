@extends('layouts.app')

@section('content')
<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-10">

            @if (session('success'))
                <div class="alert alert-success" role="alert">
                    {{ session('success') }}
                </div>
            @endif

            @if (session('error'))
                <div class="alert alert-danger" role="alert">
                    {{ session('error') }}
                </div>
            @endif

            <div class="card mb-3">
                <div class="card-header">{{ __('Topics') }}</div>

                <div class="card-body">
                    @can('create', App\Models\Topic::class)
                    <a class="btn btn-primary mb-3" href="{{ route('topics.create') }}" role="button">Create a topic</a>
                    @endcan

                    <div class="card-group">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">Name</th>
                                    <th scope="col">Action</th>
                                    <th scope="col">Votes</th>
                                    <th scope="col">Created at</th>
                                    <th scope="col">Closed at</th>
                                    <th scope="col">Operations</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($topics as $topic)
                                <tr>
                                    <th scope="row"><a href="{{ route('topics.show', [ 'id' => $topic->id ]) }}">{{ $topic->title }}</a></th>
                                    <td>{{ ucfirst($topic->type) }}</td>
                                    <td>{{ $topic->votes()->count() }}</td>
                                    <td>{{ $topic->created_at->format('Y-m-d') }}</td>
                                    <td>{{ $topic->closed_at->format('Y-m-d') }}</td>
                                    <td>
                                        @can('update', $topic)
                                        <a class="btn btn-info btn-sm" role="button" href="{{ route('topics.edit', ['id' => $topic->id]) }}">Edit</a>
                                        @endcan
                                        @can('delete', $topic)
                                        <form style="display:inline-block" method="POST" action="{{ route('topics.destroy', ['id' => $topic->id]) }}" aria-label="{{ __('Delete topic') }}">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-danger btn-sm" type="submit">Delete</button>
                                        </form>
                                        @endcan
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                    {{ $topics->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
