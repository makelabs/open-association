@extends('layouts.app')

@section('content')
<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-10">

            @if (session('success'))
                <div class="alert alert-success" role="alert">
                    {{ session('success') }}
                </div>
            @endif

            @if (session('error'))
                <div class="alert alert-danger" role="alert">
                    {{ session('error') }}
                </div>
            @endif

            {{ Breadcrumbs::render('user', $user) }}

            <div class="card mb-3">

                <div class="card-body">
                    <dl class="row">
                        <dt class="col-sm-2">Name</dt>
                        <dd class="col-sm-10">{{ $user->name }}</dd>

                        <dt class="col-sm-2">Username</dt>
                        <dd class="col-sm-10">{{ $user->username }}</dd>

                        <dt class="col-sm-2">Email</dt>
                        <dd class="col-sm-10">{{ $user->email }}</dd>

                        <dt class="col-sm-2">Roles</dt>
                        <dd class="col-sm-10">
                            @foreach ($user->roles as $role)
                            <span class="badge badge-primary">{{ $role->name }}</span>
                            @endforeach
                        </dd>

                        <dt class="col-sm-2">Updated at</dt>
                        <dd class="col-sm-10">{{ $user->updated_at->format('Y-m-d H:i:s') }}</dd>

                        <dt class="col-sm-2">Created at</dt>
                        <dd class="col-sm-10">{{ $user->created_at->format('Y-m-d H:i:s') }}</dd>

                        <dt class="col-sm-2">Activated</dt>
                        <dd class="col-sm-10">{{ $user->confirm_token == null ? 'Yes' : 'No' }}</dd>
                    </dl>
                    <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
                        @can('update', $user)
                        <div class="btn-group mr-2" role="group" aria-label="Edit user">
                            <a class="btn btn-info" role="button" href="{{ route('admin.users.edit', ['id' => $user->id]) }}">Edit</a>
                        </div>
                        @endcan
                        @can('delete', $user)
                        <div class="btn-group" role="group" aria-label="Delete user">
                            <form style="display:inline-block" method="POST" action="{{ route('admin.users.destroy', ['id' => $user->id]) }}" aria-label="{{ __('Delete user') }}">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger" type="submit">Delete</button>
                            </form>
                        </div>
                        @endcan
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
