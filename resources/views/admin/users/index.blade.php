@extends('layouts.app')

@section('content')
<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-10">

            @if (session('success'))
                <div class="alert alert-success" role="alert">
                    {{ session('success') }}
                </div>
            @endif

            @if (session('error'))
                <div class="alert alert-danger" role="alert">
                    {{ session('error') }}
                </div>
            @endif

            <div class="card mb-3">
                <div class="card-header">{{ __('Users') }}</div>

                <div class="card-body">
                    @can('create', App\Models\User::class)
                    <a class="btn btn-primary mb-3" href="{{ route('admin.users.create') }}" role="button">Add user</a>
                    @endcan

                    <div class="card-group">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">Name</th>
                                    <th scope="col">Username</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Roles</th>
                                    <th scope="col">Created at</th>
                                    <th scope="col">Operations</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($users as $user)
                                <tr>
                                    <th scope="row"><a href="{{ route('admin.users.show', [ 'id' => $user->id ]) }}">{{ $user->name }}</a></th>
                                    <td>{{ $user->username }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>
                                        @foreach ($user->roles as $role)
                                            <a href="{{ route('admin.roles.show', ['id' => $role->id]) }}" class="badge badge-primary">{{ ucfirst($role->name) }}</a>
                                        @endforeach
                                    </td>
                                    <td>{{ $user->created_at->format('Y-m-d') }}</td>
                                    <td>
                                        @can('update', $user)
                                        <a class="btn btn-info btn-sm" role="button" href="{{ route('admin.users.edit', ['id' => $user->id]) }}">Edit</a>
                                        @endcan
                                        @can('delete', $user)
                                        <form style="display:inline-block" method="POST" action="{{ route('admin.users.destroy', ['id' => $user->id]) }}" aria-label="{{ __('Delete user') }}">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-danger btn-sm" type="submit">Delete</button>
                                        </form>
                                        @endcan
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                    {{ $users->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
