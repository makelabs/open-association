@extends('layouts.app')

@section('content')
<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-10">

            {{ Breadcrumbs::render('role', $role) }}

            <div class="card mb-3">

                <div class="card-header">{{ __('Edit role') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('admin.roles.update', ['id' => $role->id]) }}" aria-label="{{ __('Edit role') }}">
                        @csrf
                        @method('PUT')

                        <input type="hidden" name="id" value="{{ $role->id }}">

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ $role->name }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="roles" class="col-md-4 col-form-label text-md-right">{{ __('Permissions') }}</label>

                            <div class="col-md-6">
                                <select multiple class="form-control{{ $errors->has('permissions') ? ' is-invalid' : '' }}" id="permissions" name="permissions[]" required>
                                @foreach ($permissions as $permission)
                                    <option value="{{ $permission->id }}" {{ $role->hasPermissionTo($permission->id) ? 'selected' : '' }}>{{ $permission->name }}</option>
                                @endforeach
                                </select>

                                @if ($errors->has('permissions'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('permissions') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Edit') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
