@extends('layouts.app')

@section('content')
<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-10">

            @if (session('success'))
                <div class="alert alert-success" role="alert">
                    {{ session('success') }}
                </div>
            @endif

            @if (session('error'))
                <div class="alert alert-danger" role="alert">
                    {{ session('error') }}
                </div>
            @endif

            {{ Breadcrumbs::render('role', $role) }}

            <div class="card mb-3">

                <div class="card-body">
                    <dl class="row">
                        <dt class="col-sm-2">Name</dt>
                        <dd class="col-sm-10">{{ ucfirst($role->name) }}</dd>

                        <dt class="col-sm-2">Permissions</dt>
                        <dd class="col-sm-10">
                            @foreach ($role->permissions as $permission)
                            <a href="{{ route('admin.permissions.show', ['id' => $permission->id]) }}" class="badge badge-primary">{{ ucfirst($permission->name) }}</a>
                            @endforeach
                        </dd>

                        <dt class="col-sm-2">Updated at</dt>
                        <dd class="col-sm-10">{{ $role->updated_at->format('Y-m-d H:i:s') }}</dd>

                        <dt class="col-sm-2">Created at</dt>
                        <dd class="col-sm-10">{{ $role->created_at->format('Y-m-d H:i:s') }}</dd>
                    </dl>
                    <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
                        @can('update', $role)
                        <div class="btn-group mr-2" role="group" aria-label="Edit role">
                            <a class="btn btn-info" role="button" href="{{ route('admin.roles.edit', ['id' => $role->id]) }}">Edit</a>
                        </div>
                        @endcan
                        @can('delete', $role)
                        <div class="btn-group" role="group" aria-label="Delete role">
                            <form style="display:inline-block" method="POST" action="{{ route('admin.roles.destroy', ['id' => $role->id]) }}" aria-label="{{ __('Delete role') }}">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger" type="submit">Delete</button>
                            </form>
                        </div>
                        @endcan
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
