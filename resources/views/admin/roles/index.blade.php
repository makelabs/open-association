@extends('layouts.app')

@section('content')
<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-10">

            @if (session('success'))
                <div class="alert alert-success" role="alert">
                    {{ session('success') }}
                </div>
            @endif

            @if (session('error'))
                <div class="alert alert-danger" role="alert">
                    {{ session('error') }}
                </div>
            @endif

            <div class="card mb-3">
                <div class="card-header">{{ __('Roles') }}</div>

                <div class="card-body">
                    @can('create', Spatie\Permission\Models\Role::class)
                    <a class="btn btn-primary mb-3" href="{{ route('admin.roles.create') }}" role="button">Add role</a>
                    @endcan

                    <div class="card-group">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">Name</th>
                                    <th scope="col">Permissions</th>
                                    <th scope="col">Users</th>
                                    <th scope="col">Created at</th>
                                    <th scope="col">Operations</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($roles as $role)
                                <tr>
                                    <td>{{ ucfirst($role->name) }}</td>
                                    <td>{{ $role->permissions->count() }}</td>
                                    <td>{{ $role->users->count() }}</td>
                                    <td>{{ $role->created_at->format('Y-m-d') }}</td>
                                    <td>
                                        <a class="btn btn-secondary btn-sm" role="button" href="{{ route('admin.roles.show', ['id' => $role->id]) }}">View</a>
                                        @can('update', $role)
                                        <a class="btn btn-info btn-sm" role="button" href="{{ route('admin.roles.edit', ['id' => $role->id]) }}">Edit</a>
                                        @endcan
                                        @can('delete', $role)
                                        <form style="display:inline-block" method="POST" action="{{ route('admin.roles.destroy', ['id' => $role->id]) }}" aria-label="{{ __('Delete role') }}">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-danger btn-sm" type="submit">Delete</button>
                                        </form>
                                        @endcan
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                    {{ $roles->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
