@extends('layouts.app')

@section('content')
<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-10">

            {{ Breadcrumbs::render('tag', $tag) }}

            <div class="card mb-3">

                <div class="card-header">{{ __('Edit tag') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('admin.tags.update', ['id' => $tag->id]) }}" aria-label="{{ __('Edit tag') }}">
                        @csrf
                        @method('PUT')

                        <input type="hidden" name="id" value="{{ $tag->id }}">

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ $tag->name }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" tag="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Edit') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
