@extends('layouts.app')

@section('content')
<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-10">

            @if (session('success'))
                <div class="alert alert-success" tag="alert">
                    {{ session('success') }}
                </div>
            @endif

            @if (session('error'))
                <div class="alert alert-danger" tag="alert">
                    {{ session('error') }}
                </div>
            @endif

            {{ Breadcrumbs::render('tag', $tag) }}

            <div class="card mb-3">

                <div class="card-body">
                    <dl class="row">
                        <dt class="col-sm-2">Name</dt>
                        <dd class="col-sm-10">{{ ucfirst($tag->name) }}</dd>

                        <dt class="col-sm-2">Updated at</dt>
                        <dd class="col-sm-10">{{ $tag->updated_at->format('Y-m-d H:i:s') }}</dd>

                        <dt class="col-sm-2">Created at</dt>
                        <dd class="col-sm-10">{{ $tag->created_at->format('Y-m-d H:i:s') }}</dd>
                    </dl>
                    <div class="btn-toolbar" tag="toolbar" aria-label="Toolbar with button groups">
                        @can('update', $tag)
                        <div class="btn-group mr-2" tag="group" aria-label="Edit tag">
                            <a class="btn btn-info" tag="button" href="{{ route('admin.tags.edit', ['id' => $tag->id]) }}">Edit</a>
                        </div>
                        @endcan
                        @can('delete', $tag)
                        <div class="btn-group" tag="group" aria-label="Delete tag">
                            <form style="display:inline-block" method="POST" action="{{ route('admin.tags.destroy', ['id' => $tag->id]) }}" aria-label="{{ __('Delete tag') }}">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger" type="submit">Delete</button>
                            </form>
                        </div>
                        @endcan
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
