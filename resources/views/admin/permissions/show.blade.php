@extends('layouts.app')

@section('content')
<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-10">

            @if (session('success'))
                <div class="alert alert-success" role="alert">
                    {{ session('success') }}
                </div>
            @endif

            @if (session('error'))
                <div class="alert alert-danger" role="alert">
                    {{ session('error') }}
                </div>
            @endif

            {{ Breadcrumbs::render('permission', $permission) }}

            <div class="card mb-3">

                <div class="card-body">
                    <dl class="row">
                        <dt class="col-sm-2">Name</dt>
                        <dd class="col-sm-10">{{ ucfirst($permission->name) }}</dd>

                        <dt class="col-sm-2">Roles</dt>
                        <dd class="col-sm-10">
                            @foreach ($permission->roles as $role)
                            <a href="{{ route('admin.roles.show', ['id' => $role->id]) }}" class="badge badge-primary">{{ ucfirst($role->name) }}</a>
                            @endforeach
                        </dd>

                        <dt class="col-sm-2">Updated at</dt>
                        <dd class="col-sm-10">{{ $permission->updated_at->format('Y-m-d H:i:s') }}</dd>

                        <dt class="col-sm-2">Created at</dt>
                        <dd class="col-sm-10">{{ $permission->created_at->format('Y-m-d H:i:s') }}</dd>
                    </dl>
                    <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
                        @can('update', $permission)
                        <div class="btn-group mr-2" role="group" aria-label="Edit permission">
                            <a class="btn btn-info" role="button" href="{{ route('admin.permissions.edit', ['id' => $permission->id]) }}">Edit</a>
                        </div>
                        @endcan
                        @can('delete', $permission)
                        <div class="btn-group" role="group" aria-label="Delete permission">
                            <form style="display:inline-block" method="POST" action="{{ route('admin.permissions.destroy', ['id' => $permission->id]) }}" aria-label="{{ __('Delete permission') }}">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger" type="submit">Delete</button>
                            </form>
                        </div>
                        @endcan
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
