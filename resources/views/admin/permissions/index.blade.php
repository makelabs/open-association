@extends('layouts.app')

@section('content')
<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-10">

            @if (session('success'))
                <div class="alert alert-success" role="alert">
                    {{ session('success') }}
                </div>
            @endif

            @if (session('error'))
                <div class="alert alert-danger" role="alert">
                    {{ session('error') }}
                </div>
            @endif

            <div class="card mb-3">
                <div class="card-header">{{ __('Permissions') }}</div>

                <div class="card-body">
                    @can('create', Spatie\Permission\Models\Permission::class)
                    <a class="btn btn-primary mb-3" href="{{ route('admin.permissions.create') }}" role="button">Add permission</a>
                    @endcan

                    <div class="card-group">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">Name</th>
                                    <th scope="col">Roles</th>
                                    <th scope="col">Created at</th>
                                    <th scope="col">Operations</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($permissions as $permission)
                                <tr>
                                    <td>{{ ucfirst($permission->name) }}</td>
                                    <td>{{ $permission->roles->count() }}</td>
                                    <td>{{ $permission->created_at->format('Y-m-d') }}</td>
                                    <td>
                                        <a class="btn btn-secondary btn-sm" role="button" href="{{ route('admin.permissions.show', ['id' => $permission->id]) }}">View</a>
                                        @can('update', $permission)
                                        <a class="btn btn-info btn-sm" role="button" href="{{ route('admin.permissions.edit', ['id' => $permission->id]) }}">Edit</a>
                                        @endcan
                                        @can('delete', $permission)
                                        <form style="display:inline-block" method="POST" action="{{ route('admin.permissions.destroy', ['id' => $permission->id]) }}" aria-label="{{ __('Delete permission') }}">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-danger btn-sm" type="submit">Delete</button>
                                        </form>
                                        @endcan
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                    {{ $permissions->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
