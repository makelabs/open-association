@extends('layouts.app')

@section('content')
<div class="container">

    <div class="jumbotron jumbotron-fluid">
        <div class="container">
            <h1 class="display-4">Forbidden</h1>
            <p class="lead">Sorry you can't do that.</p>
        </div>
    </div>

</div>
@endsection
