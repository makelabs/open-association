@extends('layouts.app')

@section('content')
<div class="container">

    <div class="jumbotron jumbotron-fluid">
        <div class="container">
            <h1 class="display-4">Not found</h1>
            <p class="lead">The page you are looking for could not be found.</p>
        </div>
    </div>

</div>
@endsection
