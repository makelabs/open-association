@extends('layouts.app')

@section('content')
<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-10">

            @if (session('success'))
                <div class="alert alert-success" role="alert">
                    {{ session('success') }}
                </div>
            @endif

            <div class="card mb-3">
                <div class="card-header">{{ __('Documents') }}</div>

                <div class="card-body">
                    <div class="card-deck">
                        @foreach ($documents as $document)
                        <div class="card mb-3" style="width: 18rem;">
                            <div class="card-body">
                                <h5 class="card-title">
                                    <a href="{{ route('documents.download', ['id' => $document->id]) }}" target="_blank">
                                        {{ $document->name }}
                                    </a>
                                </h5>
                                @foreach ($document->tags as $tag)
                                    <a href="{{ route('tags.show', ['slug' => $tag->slug]) }}" class="badge badge-primary">{{ $tag->name }}</a>
                                @endforeach
                                <p class="card-text"> {{ $document->description }}</p>
                            </div>
                            <div class="card-footer">
                                <small class="text-muted">Last updated {{ $document->updated_at->diffForHumans() }}</small>
                                </br>
                                <small class="text-muted">Created by {{ $document->owner->name }}</small>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>

            <div class="card mb-3">
                <div class="card-header">{{ __('Topics') }}</div>

                <div class="card-body">
                    <div class="card-deck">
                        @foreach ($topics as $topic)
                        <div class="card mb-3" style="width: 18rem;">
                            <div class="card-body">
                                <h5 class="card-title">
                                    <a href="{{ route('topics.show', ['id' => $topic->id]) }}" target="_blank">
                                        {{ $topic->title }}
                                    </a>
                                </h5>
                                @foreach ($topic->tags as $tag)
                                    <a href="{{ route('tags.show', ['slug' => $tag->slug]) }}" class="badge badge-primary">{{ $tag->name }}</a>
                                @endforeach
                                <p class="card-text"> {{ $topic->description }}</p>
                            </div>
                            <div class="card-footer">
                                <small class="text-muted">Last updated {{ $topic->updated_at->diffForHumans() }}</small>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
