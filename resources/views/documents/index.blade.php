@extends('layouts.app')

@section('content')
<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-10">

            @if (session('success'))
                <div class="alert alert-success" role="alert">
                    {{ session('success') }}
                </div>
            @endif

            <div class="card mb-3">
                <div class="card-header">{{ __('Documents') }}</div>

                <div class="card-body">
                    @can('create', App\Models\Document::class)
                    <a class="btn btn-primary mb-3" href="{{ route('documents.create') }}" role="button">Create a document</a>
                    @endcan

                    @foreach ($chunks as $chunk)
                    <div class="card-deck">
                        @foreach ($chunk as $document)
                        <div class="card mb-3" style="width: 18rem;">
                            <div class="card-body">
                                <h5 class="card-title">
                                    <a href="{{ route('documents.download', ['id' => $document->id]) }}" target="_blank">
                                        {{ $document->name }}
                                    </a>
                                </h5>
                                @foreach ($document->tags as $tag)
                                    <a href="{{ route('tags.show', ['slug' => $tag->slug]) }}" class="badge badge-primary">{{ $tag->name }}</a>
                                @endforeach
                                <p class="card-text"> {{ $document->description }}</p>
                                @can('update', $document)
                                <a class="btn btn-info btn-sm" role="button" href="{{ route('documents.edit', ['id' => $document->id]) }}">Edit</a>
                                @endcan
                                @can('delete', $document)
                                <form style="display:inline-block" method="POST" action="{{ route('documents.destroy', ['id' => $document->id]) }}" aria-label="{{ __('Delete user') }}">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-danger btn-sm" type="submit">Delete</button>
                                </form>
                                @endcan
                            </div>
                            <div class="card-footer">
                                <small class="text-muted">Last updated {{ $document->updated_at->diffForHumans() }}</small>
                                </br>
                                <small class="text-muted">Created by {{ $document->owner->name }}</small>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    @endforeach

                    {{ $documents->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
