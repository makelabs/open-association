<?php

return [

    /**
     * System roles
     * These roles can not be deleted.
     */
    'roles' => [
        'admin',
        'member'
    ],

    /**
     * System permissions
     * These permissions can not be deleted.
     */
    'permissions' => [
        'viewUsers',
        'createUsers',
        'editUsers',
        'deleteUsers',

        'viewRoles',
        'createRoles',
        'editRoles',
        'deleteRoles',

        'viewPermissions',
        'createPermissions',
        'editPermissions',
        'deletePermissions',

        'viewDocuments',
        'createDocuments',
        'editDocuments',
        'deleteDocuments',

        'viewTopics',
        'createTopics',
        'editTopics',
        'deleteTopics',

        'viewTags',
        'createTags',
        'editTags',
        'deleteTags',
    ],

    /**
     * System topics
     */
    'topics' => [
        'poll' => [
            'enable' => true,
            'options' => [],
        ],
        'proposal' => [
            'enable' => true,
            'options' => [
                'agree',
                'disagree',
                'abstain',
                'block',
            ],
        ],
    ],
];
