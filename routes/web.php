<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')
    ->name('home');

Route::get('register/confirm/{token}', 'ConfirmEmailController@index')
    ->name('register.confirm');

Route::prefix('profile')->name('profile.')->group(
    function () {
        Route::get('/', 'ProfileController@index')->name('index');
        Route::get('confirm', 'ProfileController@resendConfirmEmail')->name('confirm');
        Route::post('details', 'ProfileController@changeDetails')->name('details');
        Route::post('email', 'ProfileController@changeEmail')->name('email');
        Route::post('password', 'ProfileController@changePassword')->name('password');
    }
);

Route::prefix('documents')->name('documents.')->group(
    function () {
        Route::get('/', 'DocumentController@index')->name('index');
        Route::post('/', 'DocumentController@store')->name('store');
        Route::get('create', 'DocumentController@create')->name('create');
        Route::put('{id}', 'DocumentController@update')->name('update');
        Route::delete('{id}', 'DocumentController@destroy')->name('destroy');
        Route::get('{id}/edit', 'DocumentController@edit')->name('edit');
        Route::get('{id}/download', 'DocumentController@download')->name('download');
    }
);

Route::resource('topics', 'TopicController');
Route::post('topics/{id}/vote', 'TopicController@vote')->name('topics.vote');

Route::get('tags/{slug}', 'TagController@showBySlug')->name('tags.show');

Route::prefix('admin')->name('admin.')->group(
    function () {
        Route::resource('users', 'UserController');
        Route::resource('roles', 'RoleController');
        Route::resource('permissions', 'PermissionController');
        Route::resource('tags', 'TagController');
    }
);
