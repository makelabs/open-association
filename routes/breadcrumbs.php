<?php

Breadcrumbs::for(
    'home',
    function ($trail) {
        $trail->push('Home', route('home'));
    }
);

Breadcrumbs::for(
    'documents',
    function ($trail) {
        $trail->parent('home');
        $trail->push('Documents', route('documents.index'));
    }
);

Breadcrumbs::for(
    'topics',
    function ($trail) {
        $trail->parent('home');
        $trail->push('Topics', route('topics.index'));
    }
);

Breadcrumbs::for(
    'topic',
    function ($trail, $topic) {
        $trail->parent('topics');
        $trail->push($topic->title, route('topics.show', $topic->id));
    }
);

Breadcrumbs::for(
    'admin',
    function ($trail) {
        $trail->push('Admin', '');
    }
);

Breadcrumbs::for(
    'users',
    function ($trail) {
        $trail->parent('admin');
        $trail->push('Users', route('admin.users.index'));
    }
);

Breadcrumbs::for(
    'user',
    function ($trail, $user) {
        $trail->parent('users');
        $trail->push($user->name, route('admin.users.show', $user->id));
    }
);

Breadcrumbs::for(
    'permissions',
    function ($trail) {
        $trail->parent('admin');
        $trail->push('Permissions', route('admin.permissions.index'));
    }
);

Breadcrumbs::for(
    'permission',
    function ($trail, $permission) {
        $trail->parent('permissions');
        $trail->push(
            $permission->name,
            route('admin.permissions.show', $permission->id)
        );
    }
);

Breadcrumbs::for(
    'roles',
    function ($trail) {
        $trail->parent('admin');
        $trail->push('Roles', route('admin.roles.index'));
    }
);

Breadcrumbs::for(
    'role',
    function ($trail, $role) {
        $trail->parent('roles');
        $trail->push(
            $role->name,
            route('admin.roles.show', $role->id)
        );
    }
);

Breadcrumbs::for(
    'tags',
    function ($trail) {
        $trail->parent('admin');
        $trail->push('Tags', route('admin.tags.index'));
    }
);

Breadcrumbs::for(
    'tag',
    function ($trail, $tag) {
        $trail->parent('tags');
        $trail->push(
            $tag->name,
            route('admin.tags.show', $tag->id)
        );
    }
);
