FROM node:10.8 as assets

WORKDIR /usr/app

COPY package.json .
COPY package-lock.json .

RUN npm install

COPY . .

RUN npm run prod

FROM php:7.1-apache

WORKDIR /var/www

RUN apt-get update -y && apt-get install -y \
    openssl \
    libzip-dev \
    zip

RUN docker-php-ext-install \
    zip

COPY --from=composer:1.7 /usr/bin/composer /usr/bin/composer

COPY . .

RUN composer install --no-interaction --no-dev --prefer-dist

COPY --from=assets /usr/app/public/css /var/www/public/css
COPY --from=assets /usr/app/public/images /var/www/public/images
COPY --from=assets /usr/app/public/js /var/www/public/js
COPY --from=assets /usr/app/public/mix-manifest.json /var/www/public/mix-manifest.json
