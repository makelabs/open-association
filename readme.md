# Open Association
[![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)
[![pipeline status](https://gitlab.com/makelabs/open-association/badges/develop/pipeline.svg)](https://gitlab.com/makelabs/open-association/commits/develop)
[![coverage report](https://gitlab.com/makelabs/open-association/badges/develop/coverage.svg)](https://gitlab.com/makelabs/open-association/commits/develop)

## Requirements

- PHP 7.1
- Composer 1.7
- MySQL 8.0

## Setup

Install dependencies via composer.

```bash
composer install
```

Customize `.env` file.
```bash
cp .env.default .env
```

Generate `APP_KEY`.

```bash
php artisan key:generate
```

Create database.

```bash
mysql -e "CREATE DATABASE IF NOT EXISTS `open-association`;"
```

Run database migrations.

```bash
php artisan migrate
```

## ToDo

- [] Implement comments to Topic model
- [] Implement like, dislikes to Topic and Comments models
- [] Allow users to subscribe to Topic model changes
- [] Translate UI components
- [] Implement public download links for Document model
- [] Implement Content Security Policy and CORS
