<?php

use Faker\Generator as Faker;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

$factory->define(
    User::class,
    function (Faker $faker) {

        return [
            'name' => $faker->name,
            'username' => $faker->userName,
            'email' => $faker->unique()->safeEmail,
            'password' => Hash::make('secret'),
            'confirm_token' => str_random(32),
            'remember_token' => str_random(10),
        ];
    }
);

$factory->afterCreatingState(
    User::class,
    'admin',
    function ($user, $faker) {
        $user->assignRole('admin');
    }
);

$factory->afterCreatingState(
    User::class,
    'member',
    function ($user, $faker) {
        $user->assignRole('member');
    }
);
