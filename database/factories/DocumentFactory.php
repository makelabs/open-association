<?php

use Faker\Generator as Faker;
use App\Models\Document;
use App\Models\User;

$factory->define(
    Document::class,
    function (Faker $faker) {
        $name = $faker->sentence();

        return [
            'name' => $name,
            'description' => $faker->text(),
            'owner_id' => function () {
                return factory(User::class)->create()->id;
            }
        ];
    }
);
