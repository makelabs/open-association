<?php

use Carbon\Carbon;
use Faker\Generator as Faker;
use App\Models\Topic;
use App\Models\TopicOption;

$factory->define(
    Topic::class,
    function (Faker $faker) {
        $title = $faker->sentence();

        return [
            'title' => $title,
            'description' => $faker->text(),
            'is_anonymous' => $faker->boolean(),
            'is_multiple' => $faker->boolean(),
            'hide_results' => $faker->boolean(),
            'closed_at' => Carbon::now()->addWeek(),
        ];
    }
);

$factory->state(
    Topic::class,
    'proposal',
    function (Faker $faker) {
        return [
            'is_multiple' => false
        ];
    }
);

$factory->afterCreatingState(
    Topic::class,
    'proposal',
    function (Topic $topic, Faker $faker) {

        $options = config('settings.topics.proposal.options');

        foreach ($request->options as $option) {
            factory(TopicOption::class)->create(
                [
                    'topic_id' => $topic->id,
                    'value' => $option
                ]
            );
        }
    }
);

$factory->afterCreatingState(
    Topic::class,
    'poll',
    function ($topic, $faker) {

    }
);
