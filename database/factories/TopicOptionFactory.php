<?php

use Faker\Generator as Faker;
use App\Models\TopicOption;
use App\Models\Topic;

$factory->define(
    TopicOption::class,
    function (Faker $faker) {
        return [
            'value' => $faker->name,
            'topic_id' => function () {
                return factory(Topic::class)->create()->id;
            }
        ];
    }
);
