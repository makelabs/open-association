<?php

use App\Models\Topic;
use App\Models\TopicOption;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TopicsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $proposalOptions = config('settings.topics.proposal.options');
        $proposalTopic = Topic::create(
            [
                'name' => 'Do you want to use this platform ?',
                'description' => 'This platform is still in an alpha stage.',
                'type' => 'proposal',
                'is_anonymous' => false,
                'is_multiple' => false,
                'closed_at' => Carbon::now()->addWeek()
            ]
        );

        foreach ($proposalOptions as $option) {
            TopicOption::create(
                [
                    'topic_id' => $proposalTopic->id,
                    'value' => $option
                ]
            );
        }

        $anonymousProposalTopic = Topic::create(
            [
                'name' => 'Do you agree to rise licence price ?',
                'description' => 'For financial reason, we need to increase licence price by 10.',
                'type' => 'proposal',
                'is_anonymous' => true,
                'is_multiple' => false,
                'closed_at' => Carbon::now()->addMonth()
            ]
        );

        foreach ($proposalOptions as $option) {
            TopicOption::create(
                [
                    'topic_id' => $anonymousProposalTopic->id,
                    'value' => $option
                ]
            );
        }

        $pollTopic = Topic::create(
            [
                'name' => 'Choose our name ?',
                'description' => 'We need a name.',
                'type' => 'topic',
                'is_anonymous' => false,
                'is_multiple' => false,
                'closed_at' => Carbon::now()->addMonth()
            ]
        );

        $pollTopicOptions = [
            'We are the champions',
            'Yes we can',
            '42'
        ];

        foreach ($pollTopicOptions as $option) {
            TopicOption::create(
                [
                    'topic_id' => $pollTopic->id,
                    'value' => $option
                ]
            );
        }

        $anonymousPollTopic = Topic::create(
            [
                'name' => 'President Election ?',
                'description' => 'We need a president.',
                'type' => 'topic',
                'is_anonymous' => true,
                'is_multiple' => false,
                'closed_at' => Carbon::now()->addWeek()
            ]
        );

        $anonymousPollTopicOptions = [
            'Sponge Bob',
            'Golum',
            'Astérix'
        ];

        foreach ($anonymousPollTopicOptions as $option) {
            TopicOption::create(
                [
                    'topic_id' => $anonymousPollTopic->id,
                    'value' => $option
                ]
            );
        }

        $multiplePollTopic = Topic::create(
            [
                'name' => 'President Election Date ?',
                'description' => 'We need a date to vote.',
                'type' => 'topic',
                'is_anonymous' => false,
                'is_multiple' => true,
                'closed_at' => Carbon::now()->addWeek()
            ]
        );

        $multiplePollTopicOptions = [
            'Never',
            'Today',
            'Tomorrow',
            'Whenever',
        ];

        foreach ($multiplePollTopicOptions as $option) {
            TopicOption::create(
                [
                    'topic_id' => $multiplePollTopic->id,
                    'value' => $option
                ]
            );
        }

        $multipleAnonymousPollTopic = Topic::create(
            [
                'name' => 'Destitute actual president ?',
                'description' => 'Do we need a new president.',
                'type' => 'topic',
                'is_anonymous' => true,
                'is_multiple' => true,
                'closed_at' => Carbon::now()->addMonth()
            ]
        );

        $multipleAnonymousPollTopicOptions = [
            'Yes',
            'No',
            'Maybe',
            'Explain',
        ];

        foreach ($multiplePollTopicOptions as $option) {
            TopicOption::create(
                [
                    'topic_id' => $multipleAnonymousPollTopic->id,
                    'value' => $option
                ]
            );
        }
    }
}
