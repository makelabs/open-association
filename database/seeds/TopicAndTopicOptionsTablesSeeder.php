<?php

use App\Models\Topic;
use App\Models\TopicOption;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Models\Tag;

class TopicAndTopicOptionsTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $proposalOptions = config('settings.topics.proposal.options');
        $proposalTopic = Topic::create(
            [
                'title' => 'Do you want to use this platform ?',
                'description' => 'This platform is still in an alpha stage.',
                'type' => 'proposal',
                'is_anonymous' => false,
                'is_multiple' => false,
                'hide_results' => false,
                'closed_at' => Carbon::now()->addWeek()
            ]
        );

        $tag = Tag::inRandomOrder()->first();
        $proposalTopic->attachTag($tag->name);

        foreach ($proposalOptions as $option) {
            TopicOption::create(
                [
                    'topic_id' => $proposalTopic->id,
                    'value' => $option
                ]
            );
        }

        $anonymousProposalTopic = Topic::create(
            [
                'title' => 'Do you agree to rise licence price ?',
                'description' => 'For financial reason, we need to increase licence price by 10.',
                'type' => 'proposal',
                'is_anonymous' => true,
                'is_multiple' => false,
                'hide_results' => false,
                'closed_at' => Carbon::now()->addMonth()
            ]
        );

        $tag = Tag::inRandomOrder()->first();
        $anonymousProposalTopic->attachTag($tag->name);

        foreach ($proposalOptions as $option) {
            TopicOption::create(
                [
                    'topic_id' => $anonymousProposalTopic->id,
                    'value' => $option
                ]
            );
        }

        $pollTopic = Topic::create(
            [
                'title' => 'Choose our name ?',
                'description' => 'We need a name.',
                'type' => 'poll',
                'is_anonymous' => false,
                'is_multiple' => false,
                'hide_results' => false,
                'closed_at' => Carbon::now()->addMonth()
            ]
        );

        $tag = Tag::inRandomOrder()->first();
        $pollTopic->attachTag($tag->name);

        $pollTopicOptions = [
            'We are the champions',
            'Yes we can',
            '42'
        ];

        foreach ($pollTopicOptions as $option) {
            TopicOption::create(
                [
                    'topic_id' => $pollTopic->id,
                    'value' => $option
                ]
            );
        }

        $anonymousPollTopic = Topic::create(
            [
                'title' => 'President Election ?',
                'description' => 'We need a president.',
                'type' => 'poll',
                'is_anonymous' => true,
                'is_multiple' => false,
                'hide_results' => false,
                'closed_at' => Carbon::now()->addWeek()
            ]
        );

        $tag = Tag::inRandomOrder()->first();
        $anonymousPollTopic->attachTag($tag->name);

        $anonymousPollTopicOptions = [
            'Sponge Bob',
            'Golum',
            'Astérix'
        ];

        foreach ($anonymousPollTopicOptions as $option) {
            TopicOption::create(
                [
                    'topic_id' => $anonymousPollTopic->id,
                    'value' => $option
                ]
            );
        }

        $multiplePollTopic = Topic::create(
            [
                'title' => 'President Election Date ?',
                'description' => 'We need a date to vote.',
                'type' => 'poll',
                'is_anonymous' => false,
                'is_multiple' => true,
                'hide_results' => false,
                'closed_at' => Carbon::now()->addWeek()
            ]
        );

        $tag = Tag::inRandomOrder()->first();
        $multiplePollTopic->attachTag($tag->name);

        $multiplePollTopicOptions = [
            'Never',
            'Today',
            'Tomorrow',
            'Whenever',
        ];

        foreach ($multiplePollTopicOptions as $option) {
            TopicOption::create(
                [
                    'topic_id' => $multiplePollTopic->id,
                    'value' => $option
                ]
            );
        }

        $multipleAnonymousPollTopic = Topic::create(
            [
                'title' => 'Destitute actual president ?',
                'description' => 'Do we need a new president.',
                'type' => 'poll',
                'is_anonymous' => true,
                'is_multiple' => true,
                'hide_results' => false,
                'closed_at' => Carbon::now()->addMonth()
            ]
        );

        $tag = Tag::inRandomOrder()->first();
        $multipleAnonymousPollTopic->attachTag($tag->name);

        $multipleAnonymousPollTopicOptions = [
            'Yes',
            'No',
            'Maybe',
            'Explain',
        ];

        foreach ($multipleAnonymousPollTopicOptions as $option) {
            TopicOption::create(
                [
                    'topic_id' => $multipleAnonymousPollTopic->id,
                    'value' => $option
                ]
            );
        }
    }
}
