<?php

use Illuminate\Database\Seeder;
use App\Models\Document;
use App\Models\User;
use App\Models\Tag;
use Illuminate\Support\Facades\Storage;
use Faker\Factory as Faker;

class DocumentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::inRandomOrder()->first();

        $document = Document::create(
            [
                'name' => 'Le guide du voyageur intergalactique',
                'description' => 'Keep calm and bring your towel',
                'owner_id' => $user->id,
            ]
        );

        $tag = Tag::inRandomOrder()->first();
        $document->attachTag($tag->name);

        $faker = Faker::create();
        $fileName = $faker->sha1() . '.txt';

        Storage::put($fileName, $faker->text());

        $filePath = storage_path('app/'. $fileName);
        $document->addMedia($filePath)->toMediaCollection('file');

        Storage::delete($fileName);
    }
}
