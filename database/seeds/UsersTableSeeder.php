<?php

use App\Models\User;
use Spatie\Permission\Models\Role;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = User::create(
            [
                'name' => 'Pablo Escobar',
                'username' => 'Narcos',
                'email' => 'pablo@example.org',
                'password' => Hash::make('secret')
            ]
        );

        $adminRole = Role::findByName('admin');
        if ($adminRole) {
            $admin->assignRole($adminRole);
        }

        $member = User::create(
            [
                'name' => 'John Doe',
                'username' => 'JD',
                'email' => 'john@example.org',
                'password' => Hash::make('secret')
            ]
        );

        $memberRole = Role::findByName('member');
        if ($memberRole) {
            $member->assignRole($memberRole);
        }
    }
}
