<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class CreatePermissionTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $tableNames = config('permission.table_names');

        Schema::create(
            $tableNames['permissions'],
            function (Blueprint $table) {
                $table->increments('id');
                $table->string('name');
                $table->string('readable_name')->nullable();
                $table->string('description')->nullable();
                $table->string('guard_name');
                $table->timestamps();
            }
        );

        Schema::create(
            $tableNames['roles'], function (Blueprint $table) {
                $table->increments('id');
                $table->string('name');
                $table->string('readable_name')->nullable();
                $table->string('description')->nullable();
                $table->string('guard_name');
                $table->timestamps();
            }
        );

        Schema::create(
            $tableNames['model_has_permissions'],
            function (Blueprint $table) use ($tableNames) {
                $table->unsignedInteger('permission_id');
                $table->morphs('model');

                $table->foreign('permission_id')
                    ->references('id')
                    ->on($tableNames['permissions'])
                    ->onDelete('cascade');

                $table->primary(
                    ['permission_id', 'model_id', 'model_type'],
                    'model_has_permissions_permission_model_type_primary'
                );
            }
        );

        Schema::create(
            $tableNames['model_has_roles'],
            function (Blueprint $table) use ($tableNames) {
                $table->unsignedInteger('role_id');
                $table->morphs('model');

                $table->foreign('role_id')
                    ->references('id')
                    ->on($tableNames['roles'])
                    ->onDelete('cascade');

                $table->primary(['role_id', 'model_id', 'model_type']);
            }
        );

        Schema::create(
            $tableNames['role_has_permissions'],
            function (Blueprint $table) use ($tableNames) {
                $table->unsignedInteger('permission_id');
                $table->unsignedInteger('role_id');

                $table->foreign('permission_id')
                    ->references('id')
                    ->on($tableNames['permissions'])
                    ->onDelete('cascade');

                $table->foreign('role_id')
                    ->references('id')
                    ->on($tableNames['roles'])
                    ->onDelete('cascade');

                $table->primary(['permission_id', 'role_id']);

                app('cache')->forget('spatie.permission.cache');
            }
        );

        $permissions = config('settings.permissions');

        foreach ($permissions as $permission) {
            Permission::create(
                [
                    'name' => $permission
                ]
            );
        }

        $roles = config('settings.roles');

        foreach ($roles as $role) {
            Role::create(
                [
                    'name' => $role
                ]
            );
        }

        $adminRole = Role::findByName('admin');
        if ($adminRole) {
            $adminRole->givePermissionTo(Permission::all());
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $tableNames = config('permission.table_names');

        Schema::drop($tableNames['role_has_permissions']);
        Schema::drop($tableNames['model_has_roles']);
        Schema::drop($tableNames['model_has_permissions']);
        Schema::drop($tableNames['roles']);
        Schema::drop($tableNames['permissions']);
    }
}
