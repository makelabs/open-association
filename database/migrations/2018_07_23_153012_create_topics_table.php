<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTopicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'topics',
            function (Blueprint $table) {
                $table->increments('id');
                $table->string('title');
                $table->string('type');
                $table->text('description');
                $table->boolean('is_anonymous');
                $table->boolean('is_multiple');
                $table->boolean('hide_results');
                $table->timestamp('closed_at');
                $table->timestamps();
            }
        );

        Schema::create(
            'topic_votes',
            function (Blueprint $table) {
                $table->increments('id');
                $table->integer('topic_id')->unsigned();
                $table->integer('voter_id')->unsigned();

                $table->foreign('topic_id')
                    ->references('id')
                    ->on('topics')
                    ->onDelete('cascade');

                $table->foreign('voter_id')
                    ->references('id')
                    ->on('users')
                    ->onDelete('cascade');

                $table->index(['topic_id', 'voter_id']);
            }
        );

        Schema::create(
            'topic_documents',
            function (Blueprint $table) {
                $table->increments('id');
                $table->integer('topic_id')->unsigned();
                $table->integer('document_id')->unsigned();
                $table->timestamps();

                $table->foreign('topic_id')
                    ->references('id')
                    ->on('topics')
                    ->onDelete('cascade');

                $table->foreign('document_id')
                    ->references('id')
                    ->on('documents');
            }
        );

        Schema::create(
            'topic_options',
            function (Blueprint $table) {
                $table->increments('id');
                $table->integer('topic_id')->unsigned();
                $table->string('value');
                $table->timestamps();

                $table->foreign('topic_id')
                    ->references('id')
                    ->on('topics')
                    ->onDelete('cascade');
            }
        );

        Schema::create(
            'topic_option_votes',
            function (Blueprint $table) {
                $table->increments('id');
                $table->integer('option_id')->unsigned();
                $table->integer('voter_id')->unsigned()->nullable();
                $table->timestamps();

                $table->foreign('option_id')
                    ->references('id')
                    ->on('topic_options')
                    ->onDelete('cascade');

                $table->foreign('voter_id')
                    ->references('id')
                    ->on('users')
                    ->onDelete('cascade');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('topic_option_votes');
        Schema::dropIfExists('topic_options');
        Schema::dropIfExists('topic_votes');
        Schema::dropIfExists('topic_documents');
        Schema::dropIfExists('topics');
    }
}
