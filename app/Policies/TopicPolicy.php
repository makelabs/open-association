<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Topic;
use Illuminate\Auth\Access\HandlesAuthorization;
use Carbon\Carbon;

class TopicPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can list topics.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function list(User $user)
    {
        if ($user->can('viewTopics')) {
            return true;
        }
    }

    /**
     * Determine whether the user can view the topic.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Topic  $topic
     * @return mixed
     */
    public function view(User $user, Topic $topic)
    {
        if ($user->can('viewTopics')) {
            return true;
        }
    }

    /**
     * Determine whether the user can create topics.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        if ($user->can('createTopics')) {
            return true;
        }
    }

    /**
     * Determine whether the user can update the topic.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Topic  $topic
     * @return mixed
     */
    public function update(User $user, Topic $topic)
    {
        if ($user->can('editTopics')) {
            return true;
        }
    }

    /**
     * Determine whether the user can delete the topic.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Topic  $topic
     * @return mixed
     */
    public function delete(User $user, Topic $topic)
    {
        if ($user->can('deleteTopics')) {
            return true;
        }
    }

    /**
     * Determine whether the vote is closed or not.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Topic  $topic
     * @return mixed
     */
    public function closed(User $user, Topic $topic)
    {
        $now = Carbon::now();

        return $now->gt($topic->closed_at);
    }

    /**
     * Determine whether the user can submit a vote to the topic.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Topic  $topic
     * @return mixed
     */
    public function vote(User $user, Topic $topic)
    {
        if ($user->hasStatedOnTopic($topic->id)) {
            return false;
        }

        if ($this->closed($user, $topic)) {
            return false;
        }

        return true;
    }

    /**
     * Determine whether the user can view results.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Topic  $topic
     * @return mixed
     */
    public function results(User $user, Topic $topic)
    {
        if ($user->hasRole('admin')) {
            return true;
        }

        if ($this->closed($user, $topic) && $topic->hide_results == true) {
            return true;
        }

        if ($topic->hide_results == false) {
            return true;
        }
    }
}
