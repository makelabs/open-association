<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Tag;
use Illuminate\Auth\Access\HandlesAuthorization;

class TagPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can list tags.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function list(User $user)
    {
        if ($user->can('viewTags')) {
            return true;
        }
    }

    /**
     * Determine whether the user can view the tag.
     *
     * @param  \App\Models\User  $user
     * @param  \Spatie\Tags\Tag  $tag
     * @return mixed
     */
    public function view(User $user, Tag $tag)
    {
        if ($user->can('viewTags')) {
            return true;
        }
    }

    /**
     * Determine whether the user can create tags.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        if ($user->can('createTags')) {
            return true;
        }
    }

    /**
     * Determine whether the user can update the tag.
     *
     * @param  \App\Models\User  $user
     * @param  \Spatie\Tags\Tag  $tag
     * @return mixed
     */
    public function update(User $user, Tag $tag)
    {
        if ($user->can('editTags')) {
            return true;
        }
    }

    /**
     * Determine whether the user can delete the tag.
     *
     * @param  \App\Models\User  $user
     * @param  \Spatie\Tags\Tag  $tag
     * @return mixed
     */
    public function delete(User $user, Tag $tag)
    {
        if ($user->can('deleteTags')) {
            return true;
        }
    }
}
