<?php

namespace App\Models;

use App\Models\Document;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'username', 'email', 'password', 'confirm_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token'
    ];

    /**
     * Check if user has admin role
     *
     * @return boolean
     */
    public function isAdmin()
    {
        return $this->hasRole('admin') == true;
    }

    /**
     * Check user is confirmed or not
     *
     * @return boolean
     */
    public function isConfirmed()
    {
        return $this->confirm_token == null;
    }

    /**
     * Confirm the user
     *
     * @return void
     */
    public function confirm()
    {
        $this->confirm_token = null;
        $this->save();
    }

    /**
     * Get the documents created by the user
     *
     * @return void
     */
    public function documents()
    {
        return $this->hasMany(Document::class, 'owner_id', 'id');
    }

    public function votes()
    {
        return $this->hasMany(TopicVote::class, 'voter_id');
    }

    /**
     * Check if user has stated on a topic
     *
     * @return boolean
     */
    public function hasStatedOnTopic($id)
    {
        $vote = $this->votes()->where('topic_id', $id)->first();

        return $vote != null;
    }

    public function detailedVoteOptions($id)
    {
        $topic = Topic::findOrfail($id);

        $topic->load('options', 'options.votes');

        $options = [];
        foreach ($topic->options as $option) {
            foreach ($option->votes->where('voter_id', $this->id) as $vote) {
                if ($vote) {
                    array_push($options, $option);
                }
            }
        }

        return $options;
    }
}
