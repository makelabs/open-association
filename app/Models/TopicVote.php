<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Topic;

class TopicVote extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'topic_id', 'voter_id',
    ];

    public function topic()
    {
        return $this->belongsTo(Topic::class, 'topic_id', 'id');
    }

    /**
     * Get the user of who submitted his vote.
     *
     * @return void
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'voter_id', 'id');
    }
}
