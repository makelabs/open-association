<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\TopicVote;
use App\Models\TopicDocument;
use App\Models\TopicOption;
use App\Models\TopicOptionVote;
use Spatie\Tags\HasTags;

class Topic extends Model
{
    use HasTags;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'description',
        'type',
        'is_anonymous',
        'is_multiple',
        'hide_results',
        'closed_at'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'closed_at' => 'datetime',
    ];

    /**
     * Get topic linked documents
     *
     * @return void
     */
    public function documents()
    {
        return $this->hasMany(TopicDocument::class, 'topic_id');
    }

    /**
     * Get topic options
     *
     * @return void
     */
    public function options()
    {
        return $this->hasMany(TopicOption::class, 'topic_id');
    }

    /**
     * Get topic votes via options
     *
     * @return void
     */
    public function votes()
    {
        return $this->hasManyThrough(
            TopicOptionVote::class,
            TopicOption::class,
            'topic_id',
            'option_id',
            'id',
            'id'
        );
    }

    /**
     * Get anonymous votes
     *
     * @return void
     */
    public function anonymousVotes()
    {
        return $this->hasMany(TopicVote::class, 'topic_id');
    }

    /**
     * Register a user vote
     *
     * @param User $user
     * @param array $choices
     * @param boolean $anonymous
     * @return void
     */
    public function vote(User $user, $choices, $anonymous)
    {

        $this->anonymousVotes()->create(
            [
                'topic_id' => $this->id,
                'voter_id' => $user->id,
            ]
        );

        foreach ($choices as $choice) {
            $option = TopicOption::where(
                [
                    'id' => $choice,
                    'topic_id' => $this->id,
                ]
            )->firstOrFail();

            $option->votes()->create(
                [
                    'option_id' => $option->id,
                    'voter_id' => ($anonymous) ? null : $user->id,
                ]
            );
        }
    }

    /**
     * Check if topic type and options are editable
     *
     * @return boolean
     */
    public function isTypeAndOptionsEditable()
    {
        return $this->votes->count() == 0;
    }

    /**
     * Sync options with topic
     *
     * @param array $options
     * @return void
     */
    public function syncOptions($options)
    {
        $this->options()->delete();
        $this->createOptions($options = null);
    }

    public function createOptions($options = null)
    {
        switch ($this->type) {
        case 'proposal':
            $options = config('settings.topics.proposal.options');
            break;
        default:
            break;
        }

        foreach ($options as $option) {
            TopicOption::create(
                [
                    'topic_id' => $this->id,
                    'value' => $option,
                ]
            );
        }
    }
}
