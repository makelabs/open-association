<?php

namespace App\Models;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Spatie\Tags\HasTags;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Document extends Model implements HasMedia
{
    use HasTags;
    use HasMediaTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'owner_id'
    ];

    /**
     * Get the user that owns the document
     *
     * @return void
     */
    public function owner()
    {
        return $this->belongsTo(User::class);
    }

    public function registerMediaCollections()
    {
        $this->addMediaCollection('file')->singleFile();
    }
}
