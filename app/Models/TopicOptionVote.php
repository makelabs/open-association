<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\TopicOption;

class TopicOptionVote extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'option_id', 'voter_id',
    ];

    public function option()
    {
        return $this->belongsTo(TopicOption::class, 'option_id', 'id');
    }

    /**
     * Get the user of who submitted his vote.
     *
     * @return void
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'voter_id', 'id');
    }
}
