<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Tags\Tag as TagModel;

class Tag extends TagModel
{
    public static function findFromSlug(string $name, string $type = null, string $locale = null)
    {
        $locale = $locale ?? app()->getLocale();

        return static::query()
            ->where("slug->{$locale}", $name)
            ->where('type', $type)
            ->first();
    }
}
