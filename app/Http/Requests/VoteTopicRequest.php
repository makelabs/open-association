<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class VoteTopicRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $this->merge(
            [
                'is_anonymous' => $this->input('is_anonymous', 'off'),
            ]
        );

        return [
            'choices' => 'required|array|min:1',
        ];
    }
}
