<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateTopicRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $this->merge(
            [
                'is_anonymous' => $this->input('is_anonymous', 'off'),
                'is_multiple' => $this->input('is_multiple', 'off'),
                'hide_results' => $this->input('hide_results', 'off'),
                'tags' => explode(',', trim($this->input('tags'), ','))
            ]
        );

        return [
            'title' => 'required|string',
            'description' => 'required|string',
            'closed_at' => 'required|date',
            'action' => function ($attribute, $value, $fail) {
                if (in_array($value, ['poll', 'proposal']) == false) {
                    return $fail(ucfirst($attribute) . ' is invalid.');
                }
            },
            'options' => 'sometimes|required|array|min:1',
            'tags' => 'sometimes|required|string|min:1',
        ];
    }
}
