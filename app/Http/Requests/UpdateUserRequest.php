<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $this->merge(['activate' => $this->input('activate', 'off')]);

        return [
            'id' => 'required|int',
            'name' => 'string|max:255',
            'username' => [
                'string',
                'max:255',
                Rule::unique('users')->ignore($this->id),
            ],
            'email' => [
                'email',
                'string',
                Rule::unique('users')->ignore($this->id),
            ],
            'password' => 'nullable|string|min:6',
            'roles' => 'array|min:1'
        ];
    }
}
