<?php

namespace App\Http\Controllers;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Http\Requests\CreateRoleRequest;
use App\Http\Requests\UpdateRoleRequest;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('list', Role::class);

        $roles = Role::paginate(15);

        return view('admin.roles.index')->with(
            [
                'roles' => $roles
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', Role::class);

        $permissions = Permission::get();

        return view('admin.roles.create')->with(
            [
                'permissions' => $permissions
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\CreateRoleRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRoleRequest $request)
    {
        $this->authorize('create', Role::class);

        $role = new Role();
        $role->name = $request->name;
        $role->save();

        $permissions = $request->permissions;
        $role->syncPermissions($permissions);

        session()->flash('success', 'Role successfully created.');
        return redirect()->route('admin.roles.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $role = Role::findOrFail($id);

        $this->authorize('view', $role);

        return view('admin.roles.show')->with(
            [
                'role' => $role
            ]
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::findOrFail($id);

        $this->authorize('update', $role);

        $permissions = Permission::all();

        return view('admin.roles.edit')->with(
            [
                'role' => $role,
                'permissions' => $permissions
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateRoleRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRoleRequest $request, $id)
    {
        $role = Role::findOrFail($id);

        $this->authorize('update', $role);

        $role->name = $request->name;
        $role->save();

        $permissions = $request->permissions;
        $role->syncPermissions($permissions);

        session()->flash('success', 'Role successfully updated.');

        return redirect()->route('admin.roles.show', ['id' => $request->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = Role::findOrFail($id);

        $this->authorize('delete', $role);

        $role->delete();

        session()->flash('success', 'Role successfully deleted.');

        return redirect()->route('admin.permissions.index');
    }
}
