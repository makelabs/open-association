<?php

namespace App\Http\Controllers;

use App\Models\Document;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\CreateDocumentRequest;
use App\Http\Requests\UpdateDocumentRequest;
use App\Models\Tag;

class DocumentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('list', Document::class);

        $documents = Document::orderBy('created_at', 'desc')->paginate(15);

        return view('documents.index')->with(
            [
                'chunks' => $documents->chunk(ceil($documents->count() / 2)),
                'documents' => $documents
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', Document::class);

        $tags = Tag::all()->pluck('name')->all();

        return view('documents.create')->with(
            [
                'tags' => $tags
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\CreateDocumentRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateDocumentRequest $request)
    {
        $this->authorize('create', Document::class);

        $file = $request->file('document');
        $path = $file->store('documents');

        $document = Document::create(
            [
                'name' => $request->name,
                'description' => $request->description,
                'owner_id' => $request->user()->id
            ]
        );

        $document->addMedia($file)->toMediaCollection('file');

        $tags = [];
        foreach ($request->tags as $tag) {
            $tag = trim($tag);
            array_push($tags, $tag);
        }
        $document->attachTags($tags);

        session()->flash('success', 'Document successfully created.');

        return redirect()->route('documents.index');
    }

    /**
     * Download stored file.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function download($id)
    {
        $document = Document::findOrFail($id);

        $this->authorize('view', $document);

        $file = $document->getFirstMedia('file');
        return response()->download($file->getPath(), $file->file_name);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $document = Document::findOrFail($id);

        $tags = Tag::all()->pluck('name')->all();

        $this->authorize('update', $document);

        return view('documents.edit')->with(
            [
                'document' => $document,
                'tags' => $tags,
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  CreateDocumentRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateDocumentRequest $request, $id)
    {
        $document = Document::findOrFail($id);

        $this->authorize('update', $document);

        $file = $request->file('document');
        if ($file) {
            $document->addMedia($file)->toMediaCollection('file');
        }

        $document->name = $request->name;
        $document->description = $request->description;
        $document->save();

        $tags = [];
        foreach ($request->tags as $tag) {
            $tag = trim($tag);
            array_push($tags, $tag);
        }

        $document->syncTags($tags);

        session()->flash('success', 'Document successfully updated.');

        return redirect()->route('documents.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $document = Document::findOrFail($id);

        $this->authorize('delete', $document);

        $document->delete();

        session()->flash('success', 'Document successfully deleted.');

        return redirect()->back();
    }
}
