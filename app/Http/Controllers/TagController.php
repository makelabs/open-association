<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Tag;
use App\Http\Requests\CreateTagRequest;
use App\Http\Requests\UpdateTagRequest;
use App\Models\Document;
use App\Models\Topic;

class TagController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('list', Tag::class);

        $tags = Tag::paginate(15);

        return view('admin.tags.index')->with(
            [
                'tags' => $tags
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', Tag::class);

        return view('admin.tags.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\CreateTagRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateTagRequest $request)
    {
        $this->authorize('create', Tag::class);

        $tag = Tag::create(['name' => $request->name]);

        session()->flash('success', 'Tag successfully created.');
        return redirect()->route('admin.tags.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tag = Tag::findOrFail($id);

        $this->authorize('view', $tag);

        return view('admin.tags.show')->with(
            [
                'tag' => $tag
            ]
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tag = Tag::findOrFail($id);

        $this->authorize('update', $tag);

        return view('admin.tags.edit')->with(
            [
                'tag' => $tag
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTagRequest $request, $id)
    {
        $tag = Tag::findOrFail($id);

        $this->authorize('update', $tag);

        $tag->name = $request->name;
        $tag->save();

        session()->flash('success', 'Tag successfully updated.');

        return redirect()->route('admin.tags.show', ['id' => $request->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tag = Tag::findOrFail($id);

        $this->authorize('delete', $tag);

        $tag->delete();

        session()->flash('success', 'Tag successfully deleted.');

        return redirect()->route('admin.tags.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showBySlug($slug)
    {
        $tag = Tag::findFromSlug($slug);

        $this->authorize('view', $tag);

        $documents = Document::withAnyTags([$tag->name])->get();
        $topics = Topic::withAnyTags([$tag->name])->get();

        return view('tags.show')->with(
            [
                'tag' => $tag,
                'documents' => $documents,
                'topics' => $topics,
            ]
        );
    }
}
