<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Topic;
use App\Http\Requests\CreateTopicRequest;
use App\Http\Requests\UpdateTopicRequest;
use App\Http\Requests\VoteTopicRequest;
use Carbon\Carbon;
use App\Models\TopicOption;
use App\Models\User;
use App\Models\Tag;

class TopicController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('list', Topic::class);

        $topics = Topic::orderBy('created_at', 'desc')->paginate(15);

        return view('topics.index')->with(
            [
                'chunks' => $topics->chunk(ceil($topics->count() / 2)),
                'topics' => $topics
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', Topic::class);

        $tags = Tag::all()->pluck('name')->all();

        return view('topics.create')->with(
            [
                'tags' => $tags
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\CreateTopicRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateTopicRequest $request)
    {
        $this->authorize('create', Topic::class);

        $isAnonymous = ($request->is_anonymous == 'on') ? true : false;
        $isMultiple = ($request->is_multiple == 'on') ? true : false;
        $hideResults = ($request->hide_results == 'on') ? true : false;
        $closedDate = Carbon::createFromFormat('Y-m-d', $request->closed_at);

        $topic = Topic::create(
            [
                'title' => $request->title,
                'description' => $request->description,
                'type' => $request->action,
                'is_anonymous' => $isAnonymous,
                'is_multiple' => $isMultiple,
                'hide_results' => $hideResults,
                'closed_at' => $closedDate,
            ]
        );

        $topic->createOptions($request->options);

        $tags = [];
        foreach ($request->tags as $tag) {
            $tag = trim($tag);
            array_push($tags, $tag);
        }
        $topic->attachTags($tags);

        session()->flash('success', 'Topic successfully updated.');

        return redirect()->route('topics.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $topic = Topic::findOrFail($id);

        $topic->load('options', 'options.votes');

        foreach ($topic->votes as $vote) {
            $vote->load('option', 'user');
        }

        $usersCount = User::all()->count();
        $votesCount = $topic->anonymousVotes()->count();

        $this->authorize('view', $topic);

        $results = null;

        foreach ($topic->options as $option) {
            $pourcentage = ($option->votes->count()) ? round(($option->votes->count() / $votesCount) * 100) : '0';
            $results[$option->value] = $pourcentage;
        }

        return view('topics.show')->with(
            [
                'topic' => $topic,
                'participation' => [
                    'pourcentage' => round(($votesCount / $usersCount) * 100),
                    'usersCount' => $usersCount,
                    'votesCount' => $votesCount,
                ],
                'userVoteOptions' => $request->user()->detailedVoteOptions($topic->id),
                'results' => $results,
            ]
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $topic = Topic::findOrFail($id);

        $this->authorize('update', $topic);

        $tags = Tag::all()->pluck('name')->all();

        return view('topics.edit')->with(
            [
                'topic' => $topic,
                'tags' => $tags
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateTopicRequest  $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTopicRequest $request, $id)
    {
        $topic = Topic::findOrFail($id);

        $this->authorize('update', $topic);

        $isAnonymous = ($request->is_anonymous == 'on') ? true : false;
        $isMultiple = ($request->is_multiple == 'on') ? true : false;
        $hideResults = ($request->hide_results == 'on') ? true : false;
        $closedDate = Carbon::createFromFormat('Y-m-d', $request->closed_at);

        $topic->title = $request->title;
        $topic->description = $request->description;
        $topic->closed_at = $closedDate;

        if ($topic->isEdisTypeAndOptionsEditableitable()) {
            $topic->type = $request->action;
            $topic->is_anonymous = $isAnonymous;
            $topic->is_multiple = $isMultiple;
            $topic->hide_results = $hideResults;

            if ($request->action == 'proposal') {
                $topic->is_multiple = false;
            }

            $topic->syncOptions($request->options);
        }

        $topic->save();

        $tags = [];
        foreach ($request->tags as $tag) {
            $tag = trim($tag);
            array_push($tags, $tag);
        }

        $topic->syncTags($tags);

        session()->flash('success', 'Topic successfully edited.');

        return redirect()->route('topics.show', ['id' => $topic->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $topic = Topic::findOrFail($id);

        $this->authorize('delete', $topic);

        $topic->delete();

        session()->flash('success', 'Topic successfully deleted.');

        return redirect()->route('topics.index');
    }

    /**
     * Register a vote for a topic
     *
     * @param \App\Http\Requests\VoteTopicRequest $request
     * @param int $id
     * @return void
     */
    public function vote(VoteTopicRequest $request, $id)
    {
        $topic = Topic::findOrFail($id);

        $this->authorize('vote', $topic);

        $isAnonymous = ($request->is_anonymous == 'on') ? true : false;

        $user = $request->user();
        $topic->vote($user, $request->choices, $isAnonymous);

        session()->flash('success', 'Statement successfully registered.');

        return redirect()->route('topics.show', ['id' => $topic->id]);
    }
}
