<?php

namespace App\Http\Controllers\Traits;

/**
 * Authorizable trait
 * Match permissions with resource methods
 */
trait Authorizable
{
    private $_abilities = [
        'index' => 'view',
        'show' => 'view',
        'download' => 'view',
        'edit' => 'edit',
        'update' => 'edit',
        'create' => 'create',
        'store' => 'create',
        'destroy' => 'delete',
    ];

    /**
     * Override of callAction to perform the authorization before
     *
     * @param [type] $method
     * @param [type] $parameters
     * @return void
     */
    public function callAction($method, $parameters)
    {
        $ability = $this->getAbility($method);

        if ($ability) {
            $this->authorize($ability);
        }

        return parent::callAction($method, $parameters);
    }

    public function getAbility($method)
    {
        $action = array_get($this->getAbilities(), $method);

        return $action ? $action . $this->permissionNamespace : null;
    }

    private function getAbilities()
    {
        return $this->_abilities;
    }

    public function setAbilities($abilities)
    {
        $this->abilities = $_abilities;
    }
}
