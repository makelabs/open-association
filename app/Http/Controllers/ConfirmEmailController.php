<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Events\Confirmed;
use Illuminate\Http\Request;

class ConfirmEmailController extends Controller
{
    /**
     * Method to render confirmation view
     *
     * @param Request $request
     * @param string $token
     * @return void
     */
    public function index(Request $request, $token = null)
    {
        $user = User::where('confirm_token', $token)->firstOrFail();

        $user->confirm();
        event(new Confirmed($user->fresh()));

        return view('auth.confirm');
    }
}
