<?php

namespace App\Http\Controllers;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Http\Requests\CreatePermissionRequest;
use App\Http\Requests\UpdatePermissionRequest;
use Illuminate\Http\Request;

class PermissionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('list', Permission::class);

        $permissions = Permission::paginate(15);

        return view('admin.permissions.index')->with(
            [
                'permissions' => $permissions
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', Permission::class);

        $roles = Role::get();

        return view('admin.permissions.create')->with(
            [
                'roles' => $roles
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\CreatePermissionRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreatePermissionRequest $request)
    {
        $this->authorize('create', Permission::class);

        $permission = new Permission();
        $permission->name = $request->name;
        $permission->save();

        $roles = $request->roles;
        $permission->syncRoles($roles);

        session()->flash('success', 'Permission successfully created.');

        return redirect()->route('admin.permissions.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $permission = Permission::findOrFail($id);

        $this->authorize('view', $permission);

        return view('admin.permissions.show')->with(
            [
                'permission' => $permission
            ]
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $permission = Permission::findOrFail($id);

        $this->authorize('update', $permission);

        return view('admin.permissions.edit')->with(
            [
                'permission' => $permission
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdatePermissionRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePermissionRequest $request, $id)
    {
        $permission = Permission::findOrFail($id);

        $this->authorize('update', $permission);

        if ($request->name) {
            $permission->name = $request->name;
            $permission->save();
        }

        $roles = $request->roles;
        $permission->syncRoles($roles);

        session()->flash('success', 'Permission successfully updated.');

        return redirect()->route('admin.permissions.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $permission = Permission::findOrFail($id);

        $this->authorize('delete', $permission);

        $permission->delete();

        session()->flash('success', 'Permission successfully deleted.');

        return redirect()->route('admin.permissions.index');
    }
}
