<?php

namespace App\Http\Controllers;

use Validator;
use Mail;
use App\Mail\ConfirmEmail;
use App\Mail\EmailChangeEmail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\UpdateProfileDetailsRequest;
use App\Http\Requests\UpdateProfilePasswordRequest;
use App\Http\Requests\UpdateProfileEmailRequest;

class ProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Method to render view of profile route
     *
     * @param \Illuminate\Http\Request $request
     * @return void
     */
    public function index(Request $request)
    {
        return view('profile')->with(
            ['user' => $request->user()]
        );
    }

    /**
     * Method to change user details from profile view
     *
     * @param UpdateProfileDetailsRequest $request
     * @return void
     */
    public function changeDetails(UpdateProfileDetailsRequest $request)
    {
        $user = $request->user();

        $user->name = $request->name;
        $user->username = $request->username;

        $user->save();

        session()->flash('status', 'Profile updated successfully.');

        return redirect('profile');
    }

    /**
     * Method to change user email from profile view
     *
     * @param UpdateProfileEmailRequest $request
     * @return void
     */
    public function changeEmail(UpdateProfileEmailRequest $request)
    {
        $user = $request->user();

        if ($user->email != $request->email) {

            Mail::to($user)->send(new EmailChangeEmail($user, $request->email));

            $user->email = $request->email;
            $user->confirm_token = str_random(32);

            $user->save();

            $user = $user->fresh();

            Mail::to($user)->send(new ConfirmEmail($user));
        }

        session()->flash('success', 'Email updated successfully.');

        return redirect('profile');
    }

    /**
     * Method to change user password from profile view
     *
     * @param UpdateProfilePasswordRequest $request
     * @return void
     */
    public function changePassword(UpdateProfilePasswordRequest $request)
    {
        $user = $request->user();

        $user->password = Hash::make($request->password);
        $user->save();

        session()->flash('success', 'Password updated successfully.');

        return redirect('profile');
    }

    /**
     * Method to allow user to resend confirmation email from profile view
     *
     * @param \Illuminate\Http\Request $request
     * @return void
     */
    public function resendConfirmEmail(Request $request)
    {
        $user = $request->user();

        if ($user->isConfirmed() == false) {
            Mail::to($user)->send(new ConfirmEmail($user));
        }

        session()->flash('success', 'Confirmation email sent successfully.');

        return redirect('profile');
    }
}
