<?php

namespace App\Listeners;

use Mail;
use App\Events\Confirmed;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Mail\WelcomeEmail;

class SendWelcomeEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Registered  $event
     * @return void
     */
    public function handle(Confirmed $event)
    {
        Mail::to($event->user)->send(new WelcomeEmail($event->user));
    }
}
