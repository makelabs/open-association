<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\Document;

class DocumentTest extends TestCase
{
    use RefreshDatabase;

    public function testDocumentOwnerOneToOneRelation()
    {
        $document = factory(Document::class)->create();

        $this->assertTrue($document->owner->id != null);
    }
}
