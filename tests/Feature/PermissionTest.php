<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\User;
use Spatie\Permission\Models\Permission;

class PermissionTest extends TestCase
{
    use RefreshDatabase;

    public function testPermissionsIndexViewWithValidUser()
    {
        $user = factory(User::class)->create();

        $user->givePermissionTo('viewPermissions');

        $response = $this->actingAs($user)->get(route('admin.permissions.index'));

        $response->assertSessionHasNoErrors();
        $response->assertOk();
    }

    public function testPermissionsIndexViewWithInvalidUser()
    {
        $user = factory(User::class)->create();

        $response = $this->actingAs($user)->get(route('admin.permissions.index'));

        $response->assertSessionHasNoErrors();
        $response->assertForbidden();
    }

    public function testPermissionShowViewWithValidUser()
    {
        $user = factory(User::class)->create();

        $user->givePermissionTo('viewPermissions');

        $permission = Permission::inRandomOrder()->get()->first();

        $response = $this->actingAs($user)->get(route('admin.permissions.show', ['id' => $permission->id]));

        $response->assertSessionHasNoErrors();
        $response->assertOk();
    }

    public function testPermissionShowViewWithInvalidUser()
    {
        $user = factory(User::class)->create();

        $permission = Permission::inRandomOrder()->get()->first();

        $response = $this->actingAs($user)->get(route('admin.permissions.show', ['id' => $permission->id]));

        $response->assertSessionHasNoErrors();
        $response->assertForbidden();
    }

    public function testPermissionCreateViewWithValidUser()
    {
        $user = factory(User::class)->create();

        $user->givePermissionTo('createPermissions');

        $response = $this->actingAs($user)->get(route('admin.permissions.create'));

        $response->assertSessionHasNoErrors();
        $response->assertOk();
    }

    public function testPermissionCreateViewWithInvalidUser()
    {
        $user = factory(User::class)->create();

        $response = $this->actingAs($user)->get(route('admin.permissions.create'));

        $response->assertSessionHasNoErrors();
        $response->assertForbidden();
    }

    public function testPermissionCreateFormWithValidInputs()
    {
        $user = factory(User::class)->create();
        $user->givePermissionTo('createPermissions');

        $data = [
            'name' => 'testingPurpose',
            'roles' => [
                'member'
            ]
        ];

        $response = $this->actingAs($user)->post(route('admin.permissions.store', $data));

        $response->assertSessionHasNoErrors();
        $response->assertRedirect(route('admin.permissions.index'));
    }

    public function testPermissionEditViewWithValidUserAndSystemPermission()
    {
        $user = factory(User::class)->create();

        $user->givePermissionTo('editPermissions');

        $permission = Permission::inRandomOrder()->get()->first();

        $response = $this->actingAs($user)->get(route('admin.permissions.edit', ['id' => $permission->id]));

        $response->assertSessionHasNoErrors();
        $response->assertForbidden();
    }

    public function testPermissionEditViewWithValidUserAndNonSystemPermission()
    {
        $user = factory(User::class)->create();

        $user->givePermissionTo('editPermissions');

        $permission = Permission::create(['name' => 'testingPurpose']);

        $response = $this->actingAs($user)->get(route('admin.permissions.edit', ['id' => $permission->id]));

        $response->assertSessionHasNoErrors();
        $response->assertOk();
    }

    public function testPermissionEditViewWithInvalidUser()
    {
        $user = factory(User::class)->create();

        $permission = Permission::inRandomOrder()->get()->first();

        $response = $this->actingAs($user)->get(route('admin.permissions.edit', ['id' => $permission->id]));

        $response->assertSessionHasNoErrors();
        $response->assertForbidden();
    }

    public function testPermissionEditFormWithValidUserAndNonSystemPermission()
    {
        $user = factory(User::class)->create();

        $user->givePermissionTo('editPermissions');

        $permission = Permission::create(['name' => 'testingPurpose']);

        $data = [
            'roles' => [
                'member'
            ]
        ];

        $response = $this->actingAs($user)->put(route('admin.permissions.update', ['id' => $permission->id]), $data);

        $response->assertSessionHasNoErrors();
        $response->assertRedirect(route('admin.permissions.index'));

        $data = [
            'name' => 'testingPurpose2'
        ];

        $response = $this->actingAs($user)->put(route('admin.permissions.update', ['id' => $permission->id]), $data);

        $response->assertSessionHasNoErrors();
        $response->assertRedirect(route('admin.permissions.index'));
    }

    public function testPermissionDeleteWithValidUserAndNonSystemPermission()
    {
        $user = factory(User::class)->create();
        $user->givePermissionTo('deletePermissions');

        $permission = Permission::create(['name' => 'testingPurpose']);

        $response = $this->actingAs($user)->delete(route('admin.permissions.destroy', ['id' => $permission->id]));

        $response->assertSessionHasNoErrors();
        $response->assertRedirect(route('admin.permissions.index'));
    }

    public function testPermissionDeleteWithValidUserAndSystemPermission()
    {
        $user = factory(User::class)->create();
        $user->givePermissionTo('deletePermissions');

        $permission = Permission::inRandomOrder()->get()->first();

        $response = $this->actingAs($user)->delete(route('admin.permissions.destroy', ['id' => $permission->id]));

        $response->assertSessionHasNoErrors();
        $response->assertForbidden();
    }

    public function testPermissionDeleteWithInvalidUser()
    {
        $user = factory(User::class)->create();

        $permission = Permission::inRandomOrder()->get()->first();

        $response = $this->actingAs($user)->delete(route('admin.permissions.destroy', ['id' => $permission->id]));

        $response->assertSessionHasNoErrors();
        $response->assertForbidden();
    }
}
