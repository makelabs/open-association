<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\User;

class HomeTest extends TestCase
{
    use RefreshDatabase;

    public function testHomeWithUnauthenticatedUser()
    {
        $response = $this->get('/');

        $response->assertRedirect(route('login'));
    }

    public function testHomeWithAuthenticatedUser()
    {
        $this->withoutExceptionHandling();

        $user = factory(User::class)->create();

        $response = $this->actingAs($user)->get(route('home'));

        $response->assertOk();

        $this->assertAuthenticated();
    }
}
