<?php

namespace Tests\Feature\Auth;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\User;

class LoginTest extends TestCase
{
    use RefreshDatabase;

    public function testLoginFormWithValidInputs()
    {
        $user = factory(User::class)->create();

        $data = [
            'email' => $user->email,
            'password' => 'secret'
        ];

        $response = $this->post(route('login'), $data);

        $response->assertRedirect(route('home'));
        $response->assertSessionHasNoErrors();

        $this->assertAuthenticated();
    }
}
