<?php

namespace Tests\Feature\Auth;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\User;
use Illuminate\Support\Facades\Notification;
use Illuminate\Auth\Notifications\ResetPassword;

class ForgotPasswordTest extends TestCase
{
    use RefreshDatabase;

    public function testForgotPasswordWithValidEmail()
    {
        Notification::fake();

        $user = factory(User::class)->create();

        $data = [
            'email' => $user->email
        ];

        $response = $this->post(route('password.email'), $data);

        $response->assertRedirect(route('home'));
        $response->assertSessionHasNoErrors();
        $response->assertSessionHas('status', 'We have e-mailed your password reset link!');

        Notification::assertSentTo(
            $user,
            ResetPassword::class,
            function ($notification) use ($user) {
                return $this->assertDatabaseHas(
                    'password_resets', [
                        'email' => $user->email
                    ]
                );
            }
        );
    }

    public function testForgotPasswordWithInvalidEmail()
    {
        Notification::fake();

        $user = factory(User::class)->make();

        $data = [
            'email' => $user->email
        ];

        $response = $this->post(route('password.email'), $data);

        $response->assertRedirect(route('home'));
        $response->assertSessionHasErrors('email');

        Notification::assertNotSentTo(
            $user,
            ResetPassword::class
        );
    }
}
