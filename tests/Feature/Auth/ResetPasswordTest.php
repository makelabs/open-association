<?php

namespace Tests\Feature\Auth;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\User;
use Illuminate\Support\Facades\Notification;
use Illuminate\Auth\Notifications\ResetPassword;

class ResetPasswordTest extends TestCase
{
    use RefreshDatabase;

    public function testResetPasswordWithValidToken()
    {
        Notification::fake();

        $user = factory(User::class)->create();

        $data = [
            'email' => $user->email
        ];

        $response = $this->post(route('password.email'), $data);

        $response->assertRedirect(route('home'));
        $response->assertSessionHasNoErrors();
        $response->assertSessionHas('status', 'We have e-mailed your password reset link!');

        $resetPasswordToken = null;

        Notification::assertSentTo(
            $user,
            ResetPassword::class,
            function ($notification) use ($user, &$resetPasswordToken) {
                $resetPasswordToken = $notification->token;
                return $this->assertDatabaseHas(
                    'password_resets', [
                        'email' => $user->email
                    ]
                );
            }
        );

        $this->assertTrue($resetPasswordToken != null);

        $data = [
            'token' => $resetPasswordToken,
            'password' => 'secret',
            'password_confirmation' => 'secret',
        ];

        $response = $this->post('password/reset', $data);
        $response->assertSessionHas('status', 'Your password has been reset!');

        $response->assertRedirect(route('home'));
    }
}
