<?php

namespace Tests\Feature\Auth;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\User;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Mail;
use Illuminate\Auth\Events\Registered;
use App\Mail\ConfirmEmail;

class RegistrationTest extends TestCase
{
    use RefreshDatabase;

    public function testRegistrationFormWithValidInputs()
    {
        Event::fake();

        $user = [
            'name' => 'John Doe',
            'username' => 'JoDo',
            'email' => 'john@example.org',
            'password' => 'password',
            'password_confirmation' => 'password',
        ];

        $response = $this->post(route('register'), $user);

        Event::assertDispatched(
            Registered::class,
            function ($event) use ($user) {
                return $event->user->name === 'John Doe';
            }
        );

        $response->assertRedirect(route('home'));
        $response->assertSessionHasNoErrors();

        $this->assertDatabaseHas(
            'users',
            [
                'email' => 'john@example.org'
            ]
        );

        $this->assertAuthenticated();
    }

    public function testConfirmationEmailIsSent()
    {
        Mail::fake();

        $user = factory(User::class)->make();

        event(new Registered($user));

        Mail::assertSent(
            ConfirmEmail::class,
            function ($mail) use ($user) {
                $filled = $mail->build();

                return $filled->user->name === $user->name;
            }
        );
    }
}
