<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use App\Models\User;
use App\Models\Document;

class DocumentTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp()
    {
        parent::setUp();

        config(
            [
                'filesystems.disks.local.root' => storage_path('framework/testing/disks/local')
            ]
        );
    }

    public function testDocumentIndexViewWithValidUserAsOwner()
    {
        Storage::fake('local');

        $file = UploadedFile::fake()->image('fake.jpg');

        $user = factory(User::class)->create();
        $document = factory(Document::class)->create(['owner_id' => $user->id]);
        $document->addMedia($file)->toMediaCollection('file');

        $response = $this->actingAs($user)
            ->get(route('documents.download', ['id' => $document->id]));

        $response->assertSessionHasNoErrors();
        $response->assertOk();
    }

    public function testDocumentIndexViewWithValidUserAsNonOwner()
    {
        $user = factory(User::class)->create();

        $user->givePermissionTo('viewDocuments');

        $response = $this->actingAs($user)->get(route('documents.index'));

        $response->assertSessionHasNoErrors();
        $response->assertOk();
    }

    public function testDocumentIndexViewWithInvalidUser()
    {
        $user = factory(User::class)->create();

        $response = $this->actingAs($user)->get(route('documents.index'));

        $response->assertSessionHasNoErrors();
        $response->assertForbidden();
    }


    public function testDocumentCreateViewWithValidUser()
    {
        $user = factory(User::class)->create();

        $user->givePermissionTo('createDocuments');

        $response = $this->actingAs($user)->get(route('documents.create'));

        $response->assertSessionHasNoErrors();
        $response->assertOk();
    }

    public function testDocumentCreateViewWithInvalidUser()
    {
        $user = factory(User::class)->create();

        $response = $this->actingAs($user)->get(route('documents.create'));

        $response->assertSessionHasNoErrors();
        $response->assertForbidden();
    }

    public function testDocumentCreationFormWithValidInputs()
    {
        Storage::fake('local');

        $file = UploadedFile::fake()->image('fake.jpg');

        $user = factory(User::class)->create();

        $user->givePermissionTo('createDocuments');

        $data = [
            'name' => 'Test Document',
            'description' => 'This is a test document',
            'document' => $file
        ];

        $response = $this->actingAs($user)->post(route('documents.store'), $data);

        $response->assertSessionHasNoErrors();
        $response->assertRedirect(route('documents.index'));
        $response->assertSessionHas('success', 'Document successfully created.');

        Storage::disk('local')->assertExists('documents/' . $file->hashName());
    }

    public function testDocumentEditViewWithInvalidUser()
    {
        Storage::fake('local');

        $file = UploadedFile::fake()->image('fake.jpg');

        $user = factory(User::class)->create();
        $owner = factory(User::class)->create();

        $document = factory(Document::class)->create(['owner_id' => $owner->id]);
        $document->addMedia($file)->toMediaCollection('file');

        $response = $this->actingAs($user)
            ->get(route('documents.edit', ['id' => $document->id]));

        $response->assertSessionHasNoErrors();
        $response->assertForbidden();
    }

    public function testDocumentEditViewWithValidUserAsOwner()
    {
        Storage::fake('local');

        $file = UploadedFile::fake()->image('fake.jpg');

        $user = factory(User::class)->create();

        $document = factory(Document::class)->create(['owner_id' => $user->id]);
        $document->addMedia($file)->toMediaCollection('file');

        $response = $this->actingAs($user)
            ->get(route('documents.edit', ['id' => $document->id]));

        $response->assertSessionHasNoErrors();
        $response->assertOk();
    }

    public function testDocumentEditViewWithValidUserAsNonOwner()
    {
        Storage::fake('local');

        $file = UploadedFile::fake()->image('fake.jpg');

        $user = factory(User::class)->create();
        $owner = factory(User::class)->create();

        $user->givePermissionTo('editDocuments');

        $document = factory(Document::class)->create(['owner_id' => $owner->id]);
        $document->addMedia($file)->toMediaCollection('file');

        $response = $this->actingAs($user)
            ->get(route('documents.edit', ['id' => $document->id]));

        $response->assertSessionHasNoErrors();
        $response->assertOk();
    }

    public function testDocumentUpdateViewWithValidUser()
    {
        Storage::fake('local');

        $file = UploadedFile::fake()->image('fake.jpg');

        $user = factory(User::class)->create();

        $user->givePermissionTo('editDocuments');

        $document = factory(Document::class)->create(['owner_id' => $user->id]);
        $document->addMedia($file)->toMediaCollection('file');

        $newFile = UploadedFile::fake()->image('fake.jpg');

        $data = [
            'name' => 'Change with another name',
            'description' => $document->description,
            'document' => $newFile,
        ];

        $response = $this->actingAs($user)
            ->put(route('documents.update', ['id' => $document->id]), $data);

        $response->assertSessionHasNoErrors();
        $response->assertSessionHas('success', 'Document successfully updated.');
        $response->assertRedirect(route('documents.index'));
    }

    public function testDocumentDownloadViewWithValidUser()
    {
        Storage::fake('local');

        $file = UploadedFile::fake()->image('fake.jpg');

        $user = factory(User::class)->create();

        $user->givePermissionTo('viewDocuments');

        $document = factory(Document::class)->create(['owner_id' => $user->id]);
        $document->addMedia($file)->toMediaCollection('file');


        $response = $this->actingAs($user)
            ->get(route('documents.download', ['id' => $document->id]));

        $response->assertSessionHasNoErrors();
        $response->assertOk();
    }

    public function testDocumentDeleteWithValidUserAsOwner()
    {
        $user = factory(User::class)->create();
        $document = factory(Document::class)->create(['owner_id' => $user->id]);

        $response = $this->actingAs($user)
            ->delete(route('documents.destroy', ['id' => $document->id]));

        $response->assertSessionHasNoErrors();
        $response->assertSessionHas('success', 'Document successfully deleted.');
        $response->assertRedirect();
    }

    public function testDocumentDeleteWithValidUserAsNonOwner()
    {
        $user = factory(User::class)->create();
        $owner = factory(User::class)->create();

        $user->givePermissionTo('deleteDocuments');
        $document = factory(Document::class)->create(['owner_id' => $owner->id]);

        $response = $this->actingAs($user)
            ->delete(route('documents.destroy', ['id' => $document->id]));

        $response->assertSessionHasNoErrors();
        $response->assertSessionHas('success', 'Document successfully deleted.');
        $response->assertRedirect();
    }
}
