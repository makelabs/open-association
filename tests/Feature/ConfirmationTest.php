<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\User;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Mail;
use App\Events\Confirmed;
use App\Mail\WelcomeEmail;

class ConfirmationTest extends TestCase
{
    use RefreshDatabase;

    public function testConfirmationRouteWithValidToken()
    {
        Event::fake();

        $user = factory(User::class)->create();

        $response = $this->get(
            route(
                'register.confirm',
                [
                    'token' => $user->confirm_token
                ]
            )
        );

        $response->assertSessionHasNoErrors();
        $response->assertOk();

        $this->assertGuest();

        Event::assertDispatched(
            Confirmed::class,
            function ($event) use ($user) {
                return $event->user->name === $user->name;
            }
        );

        $user->refresh();
        $this->assertTrue($user->confirm_token == null);
    }

    public function testConfirmationRouteWithInvalidToken()
    {
        Event::fake();

        $user = factory(User::class)->create();

        $response = $this->get(
            route(
                'register.confirm',
                [
                    'token' => 'INVALIDTOKEN'
                ]
            )
        );

        $response->assertNotFound();

        $this->assertGuest();

        Event::assertNotDispatched(Confirmed::class);

        $user->refresh();
        $this->assertTrue($user->confirm_token != null);
    }

    public function testWelcomeEmailIsSent()
    {
        Mail::fake();

        $user = factory(User::class)->make();

        event(new Confirmed($user));

        Mail::assertSent(
            WelcomeEmail::class,
            function ($mail) use ($user) {
                $filled = $mail->build();

                return $filled->user->name === $user->name;
            }
        );
    }
}
