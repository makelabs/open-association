<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\Browser\Pages\RegisterPage;

class RegistrationTest extends DuskTestCase
{
    use DatabaseMigrations;

    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testRegisterForm()
    {

        $this->browse(
            function (Browser $browser) {

                $password = 'password';

                $user = factory(\App\Models\User::class)->make(
                    [
                        'password' => Hash::make($password)
                    ]
                );

                $browser->visit(new RegisterPage)
                    ->registerUser(
                        $user->name,
                        $user->username,
                        $user->email,
                        $password,
                        $password
                    )
                    ->assertPathIs('/')
                    ->assertSee('You are logged in!');
            }
        );

    }
}
