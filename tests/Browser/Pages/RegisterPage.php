<?php

namespace Tests\Browser\Pages;

use Laravel\Dusk\Browser;

class RegisterPage extends Page
{
    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return '/register';
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathIs($this->url());
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@name' => 'input[name=name]',
            '@username' => 'input[name=username]',
            '@email' => 'input[name=email]',
            '@password' => 'input[name=password]',
            '@confirm' => 'input[name=password_confirmation]',
            '@submit' => 'button[type=submit]'

        ];
    }

    /**
     * Register a new user.
     *
     * @param  Browser $browser
     * @param  string  $name
     * @param  string  $username
     * @param  string  $email
     * @param  string  $password
     * @return void
     */
    public function registerUser(Browser $browser, $name, $username, $email, $password)
    {
        $browser
            ->type('@name', $name)
            ->type('@username', $username)
            ->type('@email', $email)
            ->type('@password', $password)
            ->type('@confirm', $password)
            ->press('@submit');
    }
}
